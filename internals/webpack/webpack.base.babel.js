/**
 * COMMON WEBPACK CONFIGURATION
 */

const path = require('path');
const webpack = require('webpack');
const npmPackage = require('../../package.json');
process.traceDeprecation = true;

module.exports = (options) => ({
  entry: options.entry,
  output: Object.assign({ // Compile into js/build.js
    path: path.resolve(process.cwd(), 'build'),
    publicPath: '/',
  }, options.output), // Merge with env dependent settings
  module: {
    loaders: [
      {
        test: /\.js$/, // Transform all .js files required somewhere with Babel
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: options.babelQuery,
      }, {
        // Do not transform vendor's CSS with CSS-modules The point is that they remain
        // in global scope. Since we require these CSS files in our JS or CSS files,
        // they will be a part of our compilation either way. So, no need for
        // ExtractTextPlugin here.
        test: /\.css$/,
        exclude: /node_modules/,
        loaders: ['style-loader', 'css-loader'],
      }, {
        // Do not transform vendor's CSS with CSS-modules The point is that they remain
        // in global scope. Since we require these CSS files in our JS or CSS files,
        // they will be a part of our compilation either way. So, no need for
        // ExtractTextPlugin here.
        test: /\.css$/,
        include: /node_modules/,
        loaders: ['style-loader', 'css-loader'],
      }, {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
      }, {
        test: /\.(jpg|png|gif|ico)$/,
        loaders: [
          'file-loader', {
            loader: 'image-webpack-loader',
            query: {
              mozjpeg: {
                progressive: true,
              },
              gifsicle: {
                interlaced: false,
              },
              optipng: {
                optimizationLevel: 4,
              },
              pngquant: {
                quality: '75-90',
                speed: 3,
              },
            },
          },
        ],
      }, {
        test: /\.html$/,
        loader: 'html-loader',
      }, {
        test: /\.json$/,
        loader: 'json-loader',
      }, {
        test: /\.(mp4|webm)$/,
        loader: 'url-loader',
        query: {
          limit: 10000,
        },
      },
    ],
  },
  plugins: options
    .plugins
    .concat([
      new webpack.ProvidePlugin({
        // make fetch available
        fetch: 'exports-loader?self.fetch!whatwg-fetch',
      }),

      // Always expose NODE_ENV to webpack, in order to use `process.env.NODE_ENV`
      // inside your code for any environment checks; UglifyJS will automatically drop
      // any unreachable code.
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV),
          UPLINK_ENDPOINT: JSON.stringify(process.env.UPLINK_ENDPOINT),
          ORBIT_IDENTITY: JSON.stringify(process.env.ORBIT_IDENTITY),
          ORBIT_IDENTITY_USER: JSON.stringify(process.env.ORBIT_IDENTITY_USER),
          ORBIT_IDENTITY_PASSWORD: JSON.stringify(process.env.ORBIT_IDENTITY_PASSWORD),
          ORBIT_IDENTITY_REALM: JSON.stringify(process.env.ORBIT_IDENTITY_REALM),
          SHOW_VERSION_INFO: JSON.stringify(process.env.SHOW_VERSION_INFO),
          ORBIT_IDENTITY_CLIENT: JSON.stringify(process.env.ORBIT_IDENTITY_CLIENT),
        },
      }),
      new webpack.NamedModulesPlugin(),
    ]),
  resolve: {
    modules: [
      'app', 'node_modules',
    ],
    alias: npmPackage._moduleAliases || {}, // eslint-disable-line
    extensions: [
      '.js', '.jsx', '.react.js',
    ],
    mainFields: ['browser', 'jsnext:main', 'main'],
  },
  devtool: options.devtool,
  target: 'web', // Make web variables accessible to webpack, e.g. window
  performance: options.performance || {},
});
