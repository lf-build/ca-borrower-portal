# FROM registry.lendfoundry.com/borrower-portal-base:v0.0.19
# ADD /package.json /tmp/package.json
# WORKDIR /tmp
# RUN npm install && rm -f .npmrc
# EXPOSE 3000
# COPY ./app /tenant
# RUN apt-get update && \
#     apt-get install ssh rsync --yes && \
#     rsync --remove-source-files -a /tenant/ /tmp/app/ && \
#     apt-get purge rsync --auto-remove --yes
# ENTRYPOINT npm run start:production

FROM registry.lendfoundry.com/borrower-portal-base:v0.0.19

ARG NPM_TOKEN
ARG SHOW_VERSION_INFO
ARG UPLINK_ENDPOINT
ARG ORBIT_IDENTITY
ARG ORBIT_IDENTITY_USER
ARG ORBIT_IDENTITY_CLIENT
ARG ORBIT_IDENTITY_PASSWORD
ARG ORBIT_IDENTITY_REALM
# ADD /.npmrc /tmp/.npmrc
ADD /package.json /tmp/package.json
WORKDIR /tmp
RUN printf "//`node -p \"require('url').parse(process.env.NPM_REGISTRY_URL || 'https://registry.npmjs.org').host\"`/:_authToken=${NPM_TOKEN}\nregistry=${NPM_REGISTRY_URL:-https://registry.npmjs.org}\n" >> ~/.npmrc
RUN npm install
RUN rm -f .npmrc
COPY ./app /tenant
RUN apt-get update && \
    apt-get install ssh rsync --yes && \
    rsync --remove-source-files -a /tenant/ /tmp/app/ && \
    apt-get purge rsync --auto-remove --yes

RUN npm run build
EXPOSE 3000

ENTRYPOINT npm run start:prod