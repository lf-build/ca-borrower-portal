/*
 * TermsAndConditions Messages
 *
 * This contains all the text for the TermsAndConditions component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.TermsAndConditions.header',
    defaultMessage: 'Terms and Conditions',
  },
  mainBody: {
    id: 'app.containers.TermsAndConditions.mainBody',
    defaultMessage: 'By signing below, each of the listed business and business owners/officers (individually and collectively, “you”) authorize Tenant Group and each of its representatives, successors, assigns and designees (“Recipients”) that may be involved with or acquire commercial loans having daily repayment features or purchases of future receivables, including without limitation the application therefore (collectively, “Transactions”) to obtain consumer or personal, business and investigative reports and other information about you, including credit card processor statements and bank statements, from one or more consumer reporting agencies, such as Transunion, Experian and Equifax, and from other credit bureaus, banks, creditors and other third parties. You also authorize CAG to transmit this application form, along with any of the foregoing information obtained in connection with this application, to any or all of the Recipients for the foregoing purposes. You also consent to the release, by any creditor or financial institution, of any information relating to any of you, to CAG and to each of the Recipients, on its own behalf. Each Applicant represents they are authorized to sign this application and agrees to do business with CAG electronically, and that electronic signatures on this application and any other documents have the same legal effect as if Applicant has signed in ink. Applicant authorizes CAG to contact you using any of the telephone numbers and/or email addresses listed on this credit application or that you subsequently provide us in connection with your account.',
  },
});
