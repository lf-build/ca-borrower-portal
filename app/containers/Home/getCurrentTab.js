export function getCurrentTabOnMissingField(basic, business, owner) {
  let currentTab = 1;
  if (basic.requestedAmount && basic.confirmPassword && basic.emailAddress && basic.firstName && basic.lastName && basic.loanTimeFrame && basic.password && basic.primaryPhone && basic.purposeOfLoan) {
    currentTab = 2;
  } else {
    return currentTab;
  }
  if (business.legalCompanyName && business.businessAddress && business.businessTaxId && business.city && business.dateEstablished && business.entityType && business.homeOwnerType && business.industry && business.postalCode && business.state && business.country) {
    currentTab = 3;
  } else {
    return currentTab;
  }
  if (owner.owners) {
    for (let i = 0; i < owner.owners.length; i += 1) {
      if (owner.owners[i].city && owner.owners[i].dateOfBirth && owner.owners[i].emailAddress && owner.owners[i].firstName && owner.owners[i].homeAddress && owner.owners[i].lastName && owner.owners[i].mobilePhone && owner.owners[i].ownership && owner.owners[i].postalCode && owner.owners[i].ssn && owner.owners[i].state && owner.owners[i].country) {
        currentTab = 4;
      } else {
        currentTab = 3;
        break;
      }
    }
  }
  return currentTab;
}
