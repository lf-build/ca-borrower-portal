/*
 * Home Messages
 *
 * This contains all the text for the Home component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Home.header',
    defaultMessage: 'New Application',
  },
  basicTab: {
    id: 'app.containers.Home.basicTab',
    defaultMessage: 'Basic',
  },
  businessTab: {
    id: 'app.containers.Home.businessTab',
    defaultMessage: 'Business',
  },
  ownerTab: {
    id: 'app.containers.Home.ownerTab',
    defaultMessage: 'Owner',
  },
  finishTab: {
    id: 'app.containers.Home.finishTab',
    defaultMessage: 'Finish',
  },
  emailAddressExist: {
    id: 'app.containers.Home.emailAddressExist',
    defaultMessage: 'Email Address Already Exists.',
  },
  passwordError: {
    id: 'app.containers.Home.passwordError',
    defaultMessage: 'Your password must be eight characters including one uppercase letter, one special character and alphanumeric characters.',
  },
  serviceError: {
    id: 'app.containers.Home.serviceError',
    defaultMessage: 'Unable to process your request currently. Please contact customer success manager for further support.',
  },
});
