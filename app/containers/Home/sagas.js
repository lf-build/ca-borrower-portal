import { put, takeLatest } from 'redux-saga/effects';
import * as actionTypes from './constants';

// Individual exports for testing
export function* defaultSaga() {
  yield takeLatest(actionTypes.DETECT_IP_ADDRESS, detectIP);
}

export function* detectIP() {
  const { ip } = yield fetch('https://api.ipify.org?format=json').then((r) => r.json());
  yield put({ type: actionTypes.IP_DETECTED_SUCCESS, ip });
}

// All sagas to be loaded
export default [
  defaultSaga,
];
