/*
 *
 * Home
 *
 */

import React, { PropTypes } from 'react';
import lodash from 'lodash';
import { connect } from 'react-redux';
import uplink, { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import { createStructuredSelector } from 'reselect';
import { injectIntl, intlShape } from 'react-intl';
import { getUTCInMilliSecond } from 'components/Helper/functions';
import Spinner from 'components/Spinner';
import { userSignedIn, loadUserProfile } from 'sagas/auth/actions';
import { browserHistory, withRouter } from 'react-router';
import Layout from 'containers/Layout';
import AlertMessage from 'components/AlertMessage';
import BasicDetail from 'components/BasicDetail';
import BusinessDetail from 'components/BusinessDetail';
import OwnerDetail from 'components/OwnerDetail';
import FinalDetail from 'components/FinalDetail';
import makeAuthSelector from 'sagas/auth/selectors';
import makeSelectHome from './selectors';
import { getCurrentTabOnMissingField } from './getCurrentTab';
import { detectIP } from './actions';
import messages from './messages';

export class Home extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    const parsedData = this.getApplicationNumberByToken();
    this.state = {
      elligibleForReapply: this.props.auth && this.props.auth.user && this.props.auth.user.isEligibleForReapply ? this.props.auth.user.isEligibleForReapply : false,
      leadSource: '',
      activeIndex: 1,
      isVisible: false,
      currentTab: 1,
      applicationNumber: '',
      applicantId: '',
      lookup: {},
      basic: {},
      business: {},
      owner: {},
      final: {},
      rememberMe: false,
      urlData: parsedData,
    };
  }
  componentWillMount() {
    if (this.props.auth) {
      this.state.rememberMe = this.props.auth.rememberMe;
    }
    // If remember me: then use the token stored in redux-store and go to dashboard
    if (this.props.auth.rememberMe && this.props.auth.user) {
      if (!this.props.auth.user.isLead && !this.props.auth.user.isEligibleForReapply) {
        browserHistory.replace('/overview');
      }
    } else if (this.props.auth.user) {
      if (!this.props.auth.user.isLead && !this.props.auth.user.isEligibleForReapply) {
        browserHistory.replace('/overview');
      }
    }
  }
  componentDidMount() {
    this.props.dispatch(detectIP());
    execute(undefined, undefined, ...'api/ca/lookup'.split('/'))
    .then(({ body }) => {
      this.state.lookup = { ...body };
      this.state.lookup.State = lodash.mapKeys(this.state.lookup.State, (v, k) => k.toUpperCase());
      this.state.lookup.CanadaStates = lodash.mapKeys(this.state.lookup.CanadaStates, (v, k) => k.toUpperCase());
      this.state.sourceType = this.getKey(this.state.lookup.SourceType, this.getLeadSource());
      this.setState(this.state);
      this.getApplicationData();
    }).catch(() => {
      // Error handling
    });
  }

  getApplicationNumberByToken = () => {
    const url = browserHistory.getCurrentLocation();
    if (url && url.query && url.query.token) {
      try {
        const token = url.query.token;
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        const response = JSON.parse(window.atob(base64));
        if (response && response.Expiration) {
          const currentDate = new Date();
          const expiryDate = new Date(response.Expiration);
          const currentDateInMilliseconds = getUTCInMilliSecond(currentDate);
          const expiryDateInMilliseconds = getUTCInMilliSecond(expiryDate);
          const isTokenExpired = currentDateInMilliseconds > expiryDateInMilliseconds;
          if (!isTokenExpired) {
            return response;
          }
        }
      } catch (e) {
        return undefined;
      }
    }
    return undefined;
  }

  getApplicationData = () => {
    // Return applicant flow/ before application submission
    if ((this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) || (this.state.urlData && this.state.urlData.EntityId)) {
      execute(undefined, undefined, ...'api/ca/get-application'.split('/'), { applicationNumber: this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber ? this.props.auth.user.applicationNumber : this.state.urlData.EntityId })
    .then(({ body }) => {
      const obj = this.getStateObj(body);
      this.state.applicantId = obj.applicantId;
      this.state.applicationNumber = obj.applicationNumber;
      this.state.sourceType = body.source.sourceType;
      this.state.basic = { ...obj.basic };
      this.state.business = { ...obj.business };
      this.state.owner = { ...obj.owner };
      this.state.final = { ...obj.final };
      this.setState(this.state);
      this.state.currentTab = this.getCurrentTab();
      this.state.activeIndex = this.state.currentTab;
      this.setState(this.state);
    }).catch(() => {
      // Error handling
    });
    }
    /* } else {
      localStorage.clear();
      this.setState({ isVisible: false, currentTab: 1, applicationNumber: '', applicantId: '', lookup: {}, basic: {}, business: {}, owner: {}, final: {} }); // eslint-disable-line
    } */
  }

  getKey = (obj, val) => Object.keys(obj).find((key) => (obj[key]).toLowerCase() === val.toLowerCase());

  getLeadSource = () => {
    const url = browserHistory.getCurrentLocation();
    if (url && url.query && url.query.lead_source) {
      return url.query.lead_source;
    }
    return 'organic';
  }

  getDateObj=(dateObj) => {
    if (dateObj !== '0001-01-01-d') {
      const dateParts = (dateObj.slice(0, -2)).split('-');
      return `${dateParts[1]}/${dateParts[2]}/${dateParts[0]}`;
    }
    return null;
  }
  
  getCurrentTab=() => {
    const basic = this.state.basic;
    const business = this.state.business;
    const owner = this.state.owner;
    const currentTab = getCurrentTabOnMissingField(basic, business, owner);
    return currentTab;
  }

  getStateObj = (values) => {
    const appObj = {
      applicantId: values.applicantId,
      applicationNumber: values.applicationNumber,
      basic: {
        requestedAmount: values.requestedAmount,
        loanTimeFrame: values.loanTimeFrame,
        purposeOfLoan: values.purposeOfLoan,
        firstName: values.contactFirstName,
        lastName: values.contactLastName,
        primaryPhone: values.phone,
        emailAddress: values.email,
        password: this.props.auth && this.props.auth.user && this.props.auth.user.email ? 'Sigma@123' : '',
        confirmPassword: this.props.auth && this.props.auth.user && this.props.auth.user.email ? 'Sigma@123' : '',
      },
      business: {
        legalCompanyName: values.legalBusinessName,
        businessAddress: values.addressLine1,
        dba: values.dba,
        businessTaxId: values.businessTaxID,
        sicCode: values.sicCode,
        city: values.city,
        country: values.country.toLowerCase(),
        state: values.state,
        industry: values.industry,
        postalCode: values.zipCode,
        dateEstablished: this.getDateObj(values.businessStartDate.day),
        homeOwnerType: values.propertyType,
        entityType: values.businessType,
      },
      owner: this.getOwnerObject(values),
      final: {
        haveExistingLoan: values.haveExistingLoan && values.haveExistingLoan === true ? 1 : 2,
        averageBankBalance: values.averageBankBalances,
        annualRevenue: values.annualRevenue,
      },
    };
    return appObj;
  }

  getOwnerObject = (values) => {
    const obj = { owners: [] };
    if (values.owners && values.owners.length > 0) {
      values.owners.map((owner) => {
        const ownerObj = {
          ownerId: owner.ownerId,
          firstName: owner.firstName ? owner.firstName : undefined,
          lastName: owner.lastName ? owner.lastName : undefined,
          mobilePhone: owner.phoneNumbers && owner.phoneNumbers.length > 0 && owner.phoneNumbers[0].phone ? owner.phoneNumbers[0].phone : undefined,
          homeAddress: owner.addresses && owner.addresses.length > 0 && owner.addresses[0].addressLine1 ? owner.addresses[0].addressLine1 : undefined,
          emailAddress: owner.emailAddresses && owner.emailAddresses.length > 0 && owner.emailAddresses[0].email ? owner.emailAddresses[0].email : undefined,
          city: owner.addresses && owner.addresses.length > 0 && owner.addresses[0].city ? owner.addresses[0].city : undefined,
          country: owner.addresses && owner.addresses.length > 0 && owner.addresses[0].country ? owner.addresses[0].country.toLowerCase() : undefined,
          state: owner.addresses && owner.addresses.length > 0 && owner.addresses[0].state ? owner.addresses[0].state : undefined,
          postalCode: owner.addresses && owner.addresses.length > 0 && owner.addresses[0].zipCode ? owner.addresses[0].zipCode : undefined,
          ownership: owner.ownershipPercentage ? owner.ownershipPercentage : undefined,
          ownershipType: owner.ownershipType ? owner.ownershipType : undefined,
          ssn: owner.ssn ? owner.ssn : undefined,
          dateOfBirth: owner.dob ? owner.dob : undefined,
        };
        obj.owners.push(ownerObj);
        return owner;
      });
      return obj;
    }
    return { owners: [{}] };
  }

  getOwnersObj = () => {
    const owners = [];
    if (this.state.owner.owners && this.state.owner.owners.length > 0) {
      this.state.owner.owners.map((owner) => {
        const newOwner = {
          ownerId: owner.ownerId,
          Addresses: [{
            AddressLine1: owner.homeAddress,
            Country: owner.country,
            City: owner.city,
            ZipCode: owner.postalCode,
            State: owner.state,
          }],
          DOB: owner.dateOfBirth,
          FirstName: owner.firstName,
          LastName: owner.lastName,
          OwnershipPercentage: owner.ownership,
          OwnershipType: '1',
          EmailAddresses: [{ Email: owner.emailAddress }],
          PhoneNumbers: [{ Phone: owner.mobilePhone }],
          SSN: owner.ssn,
        };
        if (owner.homeAddress !== undefined || owner.dateOfBirth !== undefined || owner.firstName !== undefined || owner.lastName !== undefined || owner.ownership !== undefined || owner.emailAddress !== undefined || owner.mobilePhone !== undefined || owner.ssn !== undefined || owner.city !== undefined || owner.country !== undefined || owner.postalCode !== undefined || owner.state !== undefined) {
          owners.push(newOwner);
        }
        return true;
      });
    }
    return owners.length > 0 ? owners : undefined;
  }
  getPayloadObj = () => ({
    Email: (this.state.basic && this.state.basic.emailAddress) ? this.state.basic.emailAddress : null,
    RequestedAmount: (this.state.basic && this.state.basic.requestedAmount) ? this.state.basic.requestedAmount : null,
    PurposeOfLoan: (this.state.basic && this.state.basic.purposeOfLoan) ? this.state.basic.purposeOfLoan : null,
    LoanTimeFrame: (this.state.basic && this.state.basic.loanTimeFrame) ? this.state.basic.loanTimeFrame : null,
    // DateNeeded: this.state.basic.dateNeeded,
    Password: this.state.basic.password,
    LegalBusinessName: this.state.business.legalCompanyName,
    DBA: this.state.business.dba,
    AddressLine1: this.state.business.businessAddress,
    City: this.state.business.city,
    State: this.state.business.state,
    Country: this.state.business.country,
    ZipCode: this.state.business.postalCode,
    // AddressType: this.state.business.addressType,
    BusinessTaxID: this.state.business.businessTaxId,
    SICCode: this.state.business.sicCode,
    BusinessStartDate: this.state.business.dateEstablished,
    BusinessType: this.state.business.entityType,
    ContactFirstName: (this.state.basic && this.state.basic.firstName) ? this.state.basic.firstName : null,
    ContactLastName: (this.state.basic && this.state.basic.lastName) ? this.state.basic.lastName : null,
    AnnualRevenue: this.state.final.annualRevenue,
    AverageBankBalances: this.state.final.averageBankBalance,
    HaveExistingLoan: (this.state.final.haveExistingLoan === '1') ? 'true' : 'false',
    Source: this.state.sourceType ? this.state.sourceType : '1',  // Organic
    Industry: this.state.business.industry,
    Phone: (this.state.basic && this.state.basic.primaryPhone) ? this.state.basic.primaryPhone : null,
    Owners: this.getOwnersObj(),
    // Signature: this.state.final.signature,
    PropertyType: this.state.business.homeOwnerType,  // Need lookup for this
    LeadOwnerId: '',
    ApplicationNumber: this.state.applicationNumber,
    ApplicantId: this.state.applicantId,
  })

  userAutoLogin= () => {
    const payload = {
      username: this.state.basic.emailAddress,
      password: this.state.urlData && this.state.urlData.EntityId ? 'Sigma@123' : this.state.basic.password,
      realm: process.env.ORBIT_IDENTITY_REALM,
      client: process.env.ORBIT_IDENTITY_CLIENT,
      rememberMe: false,
    };
    execute(undefined, undefined, ...'authorization/identity/login'.split('/'), payload)
    .then(({ body }) => {
      this.props.dispatch(userSignedIn(body.token, body.rememberMe, body.userId));
      this.props.dispatch(loadUserProfile({ userId: body.userId }));
      uplink.config.defaultToken = body.token;
      if (this.state.urlData && this.state.urlData.EntityId) {
        setTimeout(() => this.changePassword(), 2000);
      }
    }).catch(() => {
      if (this.state.urlData) {
        this.state.currentTab = 1;
        this.state.activeIndex = 1;
        this.setState(this.state);
      }
    });
  }

  changePassword = () => {
    const payload = {
      password: this.state.basic.password,
      oldPassword: 'Sigma@123',
    };
    execute(undefined, undefined, ...'api/user/set-password'.split('/'), payload)
    .then().catch();
  }

  payloadGenerator=(values, callFrom) => {
    if (callFrom === 'basic') {
      this.state.basic = { ...values };
      this.setState(this.state);
    } else if (callFrom === 'business') {
      this.state.business = { ...values };
      this.setState(this.state);
    } else if (callFrom === 'owner') {
      this.state.owner = { ...values };
      this.setState(this.state);
    } else if (callFrom === 'final') {
      this.state.final = { ...values };
      this.setState(this.state);
    }
    const PayloadObj = this.getPayloadObj();
    if (this.props.auth && this.props.auth.user && this.props.auth.user.isEligibleForReapply) {
      PayloadObj.IsReapply = this.props.auth.user.isEligibleForReapply;
    }
    return PayloadObj;
  }

  updateStateAfterSubmit=(err, data, callFrom) => {
    const { formatMessage } = this.props.intl;
    if (err) {
      if (err && err.status && err.status.code && err.status.code === 409) {
        this.setState({ isError: true, message: formatMessage(messages.emailAddressExist), isVisible: true, showSpinner: false });
      } else if (err && err.body && err.body.message && err.body.message === 'Invalid Your password must be eight characters including one uppercase letter, one special character and alphanumeric characters.') {
        this.setState({ isError: true, message: formatMessage(messages.passwordError), isVisible: true, showSpinner: false });
      } else {
        this.setState({ isError: true, message: formatMessage(messages.serviceError), isVisible: true, showSpinner: false });
      }
    } else if (data) {
      let redirect = false;
      if (callFrom === 'basic') {
        if (this.props.auth && this.props.auth.user && this.props.auth.user.isEligibleForReapply) {
          this.state.currentTab = 2;
          this.state.activeIndex = 2;
        } else {
          setTimeout(this.userAutoLogin(), 500);
          this.state.currentTab = 2;
          this.state.activeIndex = 2;
        }
      } else if (callFrom === 'business') {
        this.state.currentTab = 3;
        this.state.activeIndex = 3;
      } else if (callFrom === 'owner') {
        this.state.currentTab = 4;
        this.state.activeIndex = 4;
      } else if (callFrom === 'final') {
        this.state.currentTab = 4;
        this.state.activeIndex = 4;
        redirect = true;
      }
      if (data.applicantId) {
        this.state.applicantId = data.applicantId;
      } else if (data.ApplicantId) {
        this.state.applicantId = data.ApplicantId;
      }
      if (data.applicationNumber) {
        this.state.applicationNumber = data.applicationNumber;
      } else if (data.ApplicantId) {
        this.state.applicationNumber = data.ApplicationNumber;
      }
      this.state.isError = false;
      this.state.isVisible = false;
      this.updateOwners(this.state, data);
      if (redirect) {
        this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId }));
        browserHistory.replace('/todo');
      }
    }
  }

  updateOwners = (currentState, data) => {
    if (data && data.owners && data.owners.length > 0) {
      const newState = currentState;
      newState.owner = this.getOwnerObject(data);
      this.setState(newState);
    } else {
      this.setState(currentState);
    }
  }

  displayTab=(index) => {
    if (index <= this.state.currentTab) {
      this.state.activeIndex = index;
      this.setState(this.state);
    }
  }

  isEmailMode = () => {
    let emailMode = true;
    if (this.state.currentTab === 1 && this.props.auth && this.props.auth.user) {
      emailMode = false;
    } else if (this.state.currentTab >= 2) {
      emailMode = false;
    }
    return emailMode;
  }

  render() {
    if (!this.state && !this.state.home) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    const emailMode = this.isEmailMode();
    return (
      <Layout emailMode={emailMode}>
        <div className="panel panel-default  ">
          <div className="panel-heading">
            <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
          </div>
          <div className="panel-body">
            <div className="row">
              <main>
                <input
                  readOnly className="tab-in" id="tab1" type="radio" name="tabs" onClick={() => this.displayTab(1)}
                  checked={(this.state.currentTab >= 1 && this.state.activeIndex === 1) ? true : ''}
                />
                <label htmlFor="tab1" className="tabbing">{formatMessage(messages.basicTab)}</label>
                <input
                  readOnly className="tab-in" id="tab2" type="radio" name="tabs" onClick={() => this.displayTab(2)}
                  checked={(this.state.currentTab >= 2 && this.state.activeIndex === 2) ? true : ''}
                />
                <label htmlFor="tab2" className="tabbing">{formatMessage(messages.businessTab)}</label>
                <input
                  readOnly className="tab-in" id="tab3" type="radio" name="tabs" onClick={() => this.displayTab(3)}
                  checked={(this.state.currentTab >= 3 && this.state.activeIndex === 3) ? true : ''}
                />
                <label htmlFor="tab3" className="tabbing">{formatMessage(messages.ownerTab)}</label>
                <input
                  readOnly className="tab-in" id="tab4" type="radio" name="tabs" onClick={() => this.displayTab(4)}
                  checked={(this.state.currentTab >= 4 && this.state.activeIndex === 4) ? true : ''}
                />
                <label htmlFor="tab4" className="tabbing">{formatMessage(messages.finishTab)}</label>
                <BasicDetail user={this.props.auth && this.props.auth.user ? this.props.auth.user : null} disableFields={(this.props.auth && this.props.auth.user && this.props.auth.user.email) || (this.state.urlData)} updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} basic={this.state.basic} payloadGenerator={this.payloadGenerator} lookup={this.state.lookup} ip={this.props.Home.ip} />
                <BusinessDetail reapplyDisableFields={this.state.elligibleForReapply} updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} business={this.state.business} payloadGenerator={this.payloadGenerator} lookup={this.state.lookup} ip={this.props.Home.ip} />
                <OwnerDetail reapplyDisableFields={this.state.elligibleForReapply} updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} owner={this.state.owner} payloadGenerator={this.payloadGenerator} lookup={this.state.lookup} ip={this.props.Home.ip} />
                <FinalDetail isEligibleForReapply={this.state.elligibleForReapply} updateStateAfterSubmit={this.updateStateAfterSubmit} dispatch={this.props.dispatch} final={this.state.final} payloadGenerator={this.payloadGenerator} ip={this.props.Home.ip} />
              </main>
            </div>
            { this.state.isVisible ? (() => <AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} />)() : <span />}
          </div>
        </div>
      </Layout>
    );
  }
}

Home.propTypes = {
  dispatch: PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  Home: PropTypes.any,
  intl: intlShape.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Home: makeSelectHome(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Home)));
