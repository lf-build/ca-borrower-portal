/*
 *
 * ToDo
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { withRouter, browserHistory } from 'react-router';
import AppLayout from 'containers/AppLayout';
import DocumentUpload from 'components/DocumentUpload';
import BankLink from 'components/BankLink';
import SignDocument from 'components/SignDocument';
import MiscellaneousDocs from 'components/MiscellaneousDocs';
import Spinner from 'components/Spinner';
import makeAuthSelector from 'sagas/auth/selectors';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';
import makeSelectToDo from './selectors';

export class ToDo extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined };
  }
  componentDidMount() {
    if (this.state.applicationNumber === undefined) {
      redirectWithReturnURL(this.props.location.pathname);
    }
  }
  render() {
    const { auth: { user } } = this.props;
    if (!user) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    return (
      <AppLayout appMode>
        { this.props.auth && this.props.auth.user && !this.props.auth.user.isDeclined ? <div className="panel-group todo-panel-group" id="accordion">
          <BankLink plaid={this.props.auth.user.plaid} applicationNumber={user.applicationNumber} />
          <DocumentUpload applicationNumber={user.applicationNumber} />
          <SignDocument applicationNumber={user.applicationNumber} />
          <MiscellaneousDocs applicationNumber={user.applicationNumber} />
          <div className="todo-box"><button className="btn btn-default" onClick={() => browserHistory.replace('/overview')}>{formatMessage(messages.doItLaterButton)}</button></div>
        </div> :
        <div className="panel-group todo-panel-group" id="accordion">
          <MiscellaneousDocs applicationNumber={user.applicationNumber} />
        </div>
     }
      </AppLayout>
    );
  }
}

ToDo.propTypes = {
  auth: React.PropTypes.object,
  location: React.PropTypes.object.isRequired,
  intl: intlShape.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ToDo: makeSelectToDo(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(ToDo)));
