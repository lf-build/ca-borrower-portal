/*
 * ToDo Messages
 *
 * This contains all the text for the ToDo component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  doItLaterButton: {
    id: 'app.containers.ToDo.doItLaterButton',
    defaultMessage: 'I\'ll do it later',
  },
});
