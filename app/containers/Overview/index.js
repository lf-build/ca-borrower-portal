/*
 *
 * Overview
 *
 */

import React from 'react';
import lodash from 'lodash';
import { connect } from 'react-redux';
import { maskBusinessTaxId } from 'lib/formatting/maskBusinessTaxId';
import { Link, browserHistory, withRouter } from 'react-router';
import { injectIntl, intlShape, FormattedNumber } from 'react-intl';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import AppLayout from 'containers/AppLayout';
import Spinner from 'components/Spinner';
import { phoneMasking, dateWithMMDDYYYY } from 'components/Helper/formatting';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import { createStructuredSelector } from 'reselect';
import makeAuthSelector from 'sagas/auth/selectors';
import decodeEntity from 'lib/decodeEntity';
import makeSelectOverview from './selectors';
import messages from './messages';

export class Overview extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined, currentTab: 1 };
  }
  componentDidMount() {
    if (this.state.applicationNumber === undefined) {
      redirectWithReturnURL(this.props.location.pathname);
      return; // eslint-disable-line
    }
    execute(undefined, undefined, ...'api/ca/overview'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
      .then(({ body }) => {
        const application = body.application;
        const lookups = body.lookups;
        application.purposeOfLoan = lookups.PurposeOfLoan[application.purposeOfLoan];
        application.loanTimeFrame = lookups.LoanTimeFrame[application.loanTimeFrame];
        application.industry = lookups.Industry[application.industry];
        application.businessType = lookups.BusinessTypes[application.businessType];
        application.propertyType = lookups.PropertyType[application.propertyType];
        application.addressType = lookups.PropertyType[application.addressType];
        this.setState({ overviewData: application });
      }).catch((e) => {
        authCheck(this.props.dispatch)(e)(this.props.location.pathname);
      });
  }
  changeCurrentTab = (tabIndex) => {
    this.setState({ currentTab: tabIndex });
  }
  maskNumber(num) {
    let maskedNum = num;
    if (num.length === 10) {
      maskedNum = phoneMasking(num);
    }
    return maskedNum;
  }

  render() {
    const { formatMessage } = this.props.intl;
    return (
      <AppLayout appMode>
        <div className="panel panel-default  ">
          <div className="panel-heading">
            <h4 className="m-t-none m-b no-margin">{formatMessage(messages.header)}</h4>
          </div>
          <div className="panel-body">
            <form role="form">
              {this.state.overviewData ?
                <div className="row">
                  <main className="overview">
                    <input className="tab-in" id="tab1" type="radio" name="tabs" checked={this.state.currentTab === 1 ? true : ''} />
                    <label htmlFor="tab1" className="tabbing"><Link onClick={() => this.changeCurrentTab(1)}>{formatMessage(messages.basic)}</Link></label>
                    <input className="tab-in" id="tab2" type="radio" name="tabs" checked={this.state.currentTab === 2 ? true : ''} />
                    <label htmlFor="tab2" className="tabbing"><Link onClick={() => this.changeCurrentTab(2)}>{formatMessage(messages.business)}</Link></label>
                    <input className="tab-in" id="tab3" type="radio" name="tabs" checked={this.state.currentTab === 3 ? true : ''} />
                    <label htmlFor="tab3" className="tabbing"><Link onClick={() => this.changeCurrentTab(3)}>{formatMessage(messages.owner)}</Link></label>
                    <input className="tab-in" id="tab4" type="radio" name="tabs" checked={this.state.currentTab === 4 ? true : ''} />
                    <label htmlFor="tab4" className="tabbing"><Link onClick={() => this.changeCurrentTab(4)}>{formatMessage(messages.finish)}</Link></label>
                    <section id="content1" className="tab-section">
                      <div className="comman_application_wraper">
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.requestedAmountLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText"><FormattedNumber {...{ value: this.state.overviewData && this.state.overviewData.requestedAmount ? this.state.overviewData.requestedAmount : '', style: 'currency', currency: 'USD', maximumFractionDigits: 2, minimumFractionDigits: 0 }} /></label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.loanTimeFrame)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.loanTimeFrame ? decodeEntity(this.state.overviewData.loanTimeFrame) : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.purposeOfFund)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.purposeOfLoan ? decodeEntity(this.state.overviewData.purposeOfLoan) : ''}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.firstNameLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.contactFirstName ? this.state.overviewData.contactFirstName : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.lastNameLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.contactLastName ? this.state.overviewData.contactLastName : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.primaryPhoneLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.phone ? this.maskNumber(this.state.overviewData.phone) : ''}</label>
                            </div>
                          </div>
                        </div>
                        <div className="comman_applicationinner business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className="form-group">
                              <label htmlFor="labelText">{formatMessage(messages.emailAddressLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.email ? this.state.overviewData.email : ''}</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <section id="content2" className="tab-section">
                      <div className="comman_application_wraper">
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.legalCompanyNameLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.legalBusinessName ? this.state.overviewData.legalBusinessName : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.dbaLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.dba && this.state.overviewData.dba !== null && this.state.overviewData.dba !== '' ? this.state.overviewData.dba : formatMessage(messages.notAvailable)}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.businessAddressLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.addressLine1 ? this.state.overviewData.addressLine1 : ''}</label>
                            </div>
                          </div>

                        </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.countryLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.country ? this.state.overviewData.country : 'N/A'}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.stateLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.state ? this.state.overviewData.state : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.cityLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.city ? this.state.overviewData.city : ''}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                        <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.postalCodeLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.zipCode ? this.state.overviewData.zipCode : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.rentOrOwn)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.propertyType ? decodeEntity(this.state.overviewData.propertyType) : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.industry)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.industry ? decodeEntity(this.state.overviewData.industry) : ''}</label>
                            </div>
                          </div>
                        </div>
                        <div className="business-details">
                        <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.businessTaxIdLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.businessTaxID ? maskBusinessTaxId(this.state.overviewData.businessTaxID) : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.entityType)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.businessType ? decodeEntity(this.state.overviewData.businessType) : ''}</label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.dateOfBirthLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.businessStartDate && this.state.overviewData.businessStartDate.time ? dateWithMMDDYYYY(this.state.overviewData.businessStartDate.time) : ''}</label>
                            </div>
                          </div>

                        </div>
                      </div>
                    </section>
                    <section id="content3" className="tab-section">
                      {this.state.overviewData && (this.state.overviewData.owners.length > 0) && this.state.overviewData.owners.map((owner) => (
                        <div className="comman_application_wraper" key={owner.ownerId}>
                          <div className="form-btn ">
                            <h4 className="heading-text">{formatMessage(messages.owner)} ({`${owner.firstName ? owner.firstName : ''} ${owner.lastName ? owner.lastName : ''}`})</h4>
                          </div>
                          <div className="comman_applicationinner business-details ">
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.firstNameLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.firstName ? owner.firstName : ''}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.lastNameLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.lastName ? owner.lastName : ''}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.homeAddress)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses && owner.addresses.length > 0 && owner.addresses[0].addressLine1 ? owner.addresses[0].addressLine1 : ''}</label>
                              </div>
                            </div>
                          </div>
                          <div className="comman_applicationinner business-details">
                          <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.countryLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses && owner.addresses.length > 0 && owner.addresses[0].country ? owner.addresses[0].country : 'N/A'} </label>
                              </div>
                              </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.stateLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses && owner.addresses.length > 0 && owner.addresses[0].state ? owner.addresses[0].state : ''}</label>
                              </div>
                            </div>
                           
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.cityLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses && owner.addresses.length > 0 && owner.addresses[0].city ? owner.addresses[0].city : ''} </label>
                              </div>
                            </div>

                          </div>
                          <div className="comman_applicationinner business-details">
                          <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.postalCodeLabel)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.addresses && owner.addresses.length > 0 && owner.addresses[0].zipCode ? owner.addresses[0].zipCode : ''} </label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.mobilePhone)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.phoneNumbers && owner.phoneNumbers.length > 0 && owner.phoneNumbers[0].phone ? this.maskNumber(owner.phoneNumbers[0].phone) : ''}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.percentageOfOwnership)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.ownershipPercentage ? owner.ownershipPercentage : ''}{formatMessage(messages.percentageOfOwnership)}</label>
                              </div>
                            </div>
                          </div>
                          <div className="comman_applicationinner business-details">
                          <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.ssn)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.ssn ? owner.ssn : ''}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.ownerEmailAddress)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.emailAddresses && owner.emailAddresses.length > 0 && owner.emailAddresses[0].email ? owner.emailAddresses[0].email : ''}</label>
                              </div>
                            </div>
                            <div className="col-md-4 col-sm-4">
                              <div className="form-group">
                                <label htmlFor="labelText">{formatMessage(messages.birthdate)}</label>
                                <label className="read-only-l" htmlFor="labelText">{owner.dob ? owner.dob : ''}</label>
                              </div>
                            </div>
                          </div>
                        </div>
                     ))}
                    </section>
                    <section id="content4" className="tab-section">
                      <div className="comman_application_wraper">
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.annualRevenueLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText"><FormattedNumber {...{ value: this.state.overviewData && this.state.overviewData.annualRevenue ? this.state.overviewData.annualRevenue : '', style: 'currency', currency: 'USD', maximumFractionDigits: 2, minimumFractionDigits: 0 }} /></label>
                            </div>
                          </div>
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.averageBankBalanceLabel)}</label>
                              <label className="read-only-l" htmlFor="labelText"><FormattedNumber {...{ value: this.state.overviewData && this.state.overviewData.averageBankBalances ? this.state.overviewData.averageBankBalances : '', style: 'currency', currency: 'USD', maximumFractionDigits: 2, minimumFractionDigits: 0 }} /></label>
                            </div>
                          </div> </div>
                        <div className="business-details">
                          <div className="col-md-4 col-sm-4">
                            <div className=" form-group">
                              <label htmlFor="labelText">{formatMessage(messages.doYouHaveLoan)}</label>
                              <label className="read-only-l" htmlFor="labelText">{this.state.overviewData && this.state.overviewData.haveExistingLoan ? 'Yes' : 'No'}</label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </main>
                  { this.props.auth && this.props.auth.user && !this.props.auth.user.isDeclined && <div className="todo-box">
                    <p>{formatMessage(messages.completeYourTodo)} <button type="button" className="btn todo-btn" onClick={() => browserHistory.replace('/todo')}>{formatMessage(messages.complete)}</button></p>
                  </div> }
                </div>
              : <Spinner />
              }
            </form>
          </div>
        </div>
      </AppLayout>
    );
  }
}

Overview.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  auth: React.PropTypes.object,
  intl: intlShape.isRequired,
  location: React.PropTypes.object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  Overview: makeSelectOverview(),
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(Overview)));
