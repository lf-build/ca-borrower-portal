/*
 * Overview Messages
 *
 * This contains all the text for the Overview component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.Overview.header',
    defaultMessage: 'Application Details',
  },
  basic: {
    id: 'app.containers.Overview.basic',
    defaultMessage: 'Basic',
  },
  business: {
    id: 'app.containers.Overview.business',
    defaultMessage: 'Business',
  },
  owner: {
    id: 'app.containers.Overview.owner',
    defaultMessage: 'Owner',
  },
  finish: {
    id: 'app.containers.Overview.finish',
    defaultMessage: 'Finish',
  },
  firstNameLabel: {
    id: 'app.containers.Overview.firstNameLabel',
    defaultMessage: 'First Name',
  },
  lastNameLabel: {
    id: 'app.containers.Overview.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  primaryPhoneLabel: {
    id: 'app.containers.Overview.primaryPhoneLabel',
    defaultMessage: 'Primary Phone',
  },
  emailAddressLabel: {
    id: 'app.containers.Overview.emailAddressLabel',
    defaultMessage: 'Username',
  },
  requestedAmountLabel: {
    id: 'app.containers.Overview.requestedAmountLabel',
    defaultMessage: 'How much do you need?',
  },
  loanTimeFrame: {
    id: 'app.containers.Overview.loanTimeFrame',
    defaultMessage: 'How soon do you need it?',
  },
  purposeOfFund: {
    id: 'app.containers.Overview.purposeOfFund',
    defaultMessage: 'Purpose of funds?',
  },
  legalCompanyNameLabel: {
    id: 'app.containers.Overview.legalCompanyNameLabel',
    defaultMessage: 'Legal Company Name',
  },
  dbaLabel: {
    id: 'app.containers.Overview.dba',
    defaultMessage: 'DBA',
  },
  businessAddressLabel: {
    id: 'app.containers.Overview.businessAddressLabel',
    defaultMessage: 'Business Address',
  },
  cityLabel: {
    id: 'app.containers.Overview.cityLabel',
    defaultMessage: 'City',
  },
  countryLabel:{
    id: 'app.containers.Overview.countryLabel',
    defaultMessage: 'Country',
  },
  stateLabel: {
    id: 'app.containers.Overview.stateLabel',
    defaultMessage: 'State',
  },
  postalCodeLabel: {
    id: 'app.containers.Overview.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  mobilePhone: {
    id: 'app.containers.Overview.mobilePhone',
    defaultMessage: 'Mobile Phone',
  },
  rentOrOwn: {
    id: 'app.containers.Overview.rentOrOwn',
    defaultMessage: 'Rent Or Own this location?',
  },
  industry: {
    id: 'app.containers.Overview.industry',
    defaultMessage: 'Industry',
  },
  businessTaxIdLabel: {
    id: 'app.containers.Overview.businessTaxIdLabel',
    defaultMessage: 'Business Tax ID',
  },
/*   sicCodeLabel: {
    id: 'app.containers.Overview.sicCodeLabel',
    defaultMessage: 'SIC Code',
  }, */
  dateOfBirthLabel: {
    id: 'app.containers.Overview.dateOfBirthLabel',
    defaultMessage: 'Date Established',
  },
  entityType: {
    id: 'app.containers.Overview.entityType',
    defaultMessage: 'Entity Type',
  },
  homeAddress: {
    id: 'app.containers.Overview.homeAddress',
    defaultMessage: 'Home Address',
  },
  percentageOfOwnership: {
    id: 'app.containers.Overview.percentageOfOwnership',
    defaultMessage: '% of Ownership',
  },
  ssn: {
    id: 'app.containers.Overview.ssn',
    defaultMessage: 'Social Security Number',
  },
  ownerEmailAddress: {
    id: 'app.containers.Overview.ownerEmailAddress',
    defaultMessage: 'Email Address',
  },
  birthdate: {
    id: 'app.containers.Overview.birthdate',
    defaultMessage: 'Birthdate',
  },
  annualRevenueLabel: {
    id: 'app.containers.Overview.annualRevenueLabel',
    defaultMessage: 'Annual Revenue',
  },
  averageBankBalanceLabel: {
    id: 'app.containers.Overview.averageBankBalanceLabel',
    defaultMessage: 'Average Bank Balance',
  },
  doYouHaveLoan: {
    id: 'app.containers.Overview.doYouHaveLoan',
    defaultMessage: 'Do you have any existing business loans or cash advance?',
  },
  signatureLabel: {
    id: 'app.containers.Overview.signatureLabel',
    defaultMessage: 'Signature',
  },
  dateNeeedLabel: {
    id: 'app.containers.Overview.dateNeeedLabel',
    defaultMessage: 'Date Needed',
  },
  addressTypeLabel: {
    id: 'app.containers.Overview.addressTypeLabel',
    defaultMessage: 'Address Type',
  },
  completeYourTodo: {
    id: 'app.containers.Overview.completeYourTodo',
    defaultMessage: 'Complete your To Do\'s',
  },
  complete: {
    id: 'app.containers.Overview.complete',
    defaultMessage: 'Complete',
  },
  notAvailable: {
    id: 'app.containers.Overview.notAvailable',
    defaultMessage: 'N/A',
  },
});
