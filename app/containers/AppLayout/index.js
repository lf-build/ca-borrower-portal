/*
 *
 * AppLayout
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import Header from 'components/Header';
import Sidenav from 'components/Sidenav';
import RightPanel from 'components/RightPanel';
import BreadCrumb from 'components/BreadCrumb';
import ApplicantSortDetails from 'components/ApplicantSortDetails';

export class AppLayout extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: React.PropTypes.node,
    emailMode: React.PropTypes.any,
    // dispatch: PropTypes.func.isRequired,
  };
  render() {
    return (
      <div
        style={{
          height: '100%',
          position: 'absolute',
          width: '100%',
        }}
      >
        <Header emailMode={this.props.emailMode} />
        <Sidenav />
        <div id="main-container">
          <BreadCrumb />
          <ApplicantSortDetails appMode={this.props.appMode} />
          <div className="page-content">
            <div className="row">
              <div className="col-sm-8">
                {React
                  .Children
                  .toArray(this.props.children)}
              </div>
              <RightPanel />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

AppLayout.defaultProps = {
  appMode: false,
};

AppLayout.propTypes = {
  appMode: React.PropTypes.bool,
};

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(null, mapDispatchToProps)(AppLayout);
