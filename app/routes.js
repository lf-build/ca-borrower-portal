// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business
import { getAsyncInjectors } from 'utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '/',
      name: 'default',
      indexRoute: { onEnter: (nextState, replace) => replace('/home') },
    }, {
      path: 'home(/:lead_source)',
      name: 'home',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Home/reducer'),
          import('containers/Home/sagas'),
          import('containers/Home'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('home', reducer.default);
          injectSagas('home', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/login(/:return_url)',
      name: 'login',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Login/reducer'),
          import('containers/Login/sagas'),
          import('containers/Login'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('login', reducer.default);
          injectSagas('login', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/forgot-password',
      name: 'forgotPassword',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ForgotPassword/reducer'),
          import('containers/ForgotPassword/sagas'),
          import('containers/ForgotPassword'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('forgotPassword', reducer.default);
          injectSagas('forgotPassword', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/profile',
      name: 'profile',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Profile/reducer'),
          import('containers/Profile/sagas'),
          import('containers/Profile'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('profile', reducer.default);
          injectSagas('profile', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/todo',
      name: 'todo',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ToDo/reducer'),
          import('containers/ToDo/sagas'),
          import('containers/ToDo'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('toDo', reducer.default);
          injectSagas('toDo', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/overview',
      name: 'overview',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Overview/reducer'),
          import('containers/Overview/sagas'),
          import('containers/Overview'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('overview', reducer.default);
          injectSagas('overview', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/change-password',
      name: 'changePassword',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ChangePassword/reducer'),
          import('containers/ChangePassword/sagas'),
          import('containers/ChangePassword'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('changePassword', reducer.default);
          injectSagas('changePassword', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'reset-account/:token/:email',
      name: 'resetAccount',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/ResetAccount/reducer'),
          import('containers/ResetAccount/sagas'),
          import('containers/ResetAccount'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('resetAccount', reducer.default);
          injectSagas('resetAccount', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/upcoming-payment',
      name: 'upcomingPayment',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/UpcomingPayment/reducer'),
          import('containers/UpcomingPayment/sagas'),
          import('containers/UpcomingPayment'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('upcomingPayment', reducer.default);
          injectSagas('upcomingPayment', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '/transactions',
      name: 'transactions',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Transactions/reducer'),
          import('containers/Transactions/sagas'),
          import('containers/Transactions'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('transactions', reducer.default);
          injectSagas('transactions', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'terms-conditions',
      name: 'termsAndConditions',
      getComponent(location, cb) {
        import('containers/TermsAndConditions')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    }, {
      path: 'drawdown',
      name: 'drawdown',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Drawdown/reducer'),
          import('containers/Drawdown/sagas'),
          import('containers/Drawdown'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('drawdown', reducer.default);
          injectSagas('drawdown', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'estimate-pay-off',
      name: 'estimatePayOff',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/EstimatePayOff/reducer'),
          import('containers/EstimatePayOff/sagas'),
          import('containers/EstimatePayOff'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('estimatePayOff', reducer.default);
          injectSagas('estimatePayOff', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'loan-detail',
      name: 'loanDetail',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/LoanDetail/reducer'),
          import('containers/LoanDetail/sagas'),
          import('containers/LoanDetail'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('loanDetail', reducer.default);
          injectSagas('loanDetail', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'make-payment',
      name: 'makePayment',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/MakePayment/reducer'),
          import('containers/MakePayment/sagas'),
          import('containers/MakePayment'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('makePayment', reducer.default);
          injectSagas('makePayment', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: 'dashboard',
      name: 'dashboard',
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/Dashboard/reducer'),
          import('containers/Dashboard/sagas'),
          import('containers/Dashboard'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('dashboard', reducer.default);
          injectSagas('dashboard', sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      name: 'notfound',
      getComponent(nextState, cb) {
        import('containers/NotFoundPage')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
