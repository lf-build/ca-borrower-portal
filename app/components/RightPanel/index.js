/**
*
* RightPanel
*
*/

import React from 'react';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class RightPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="col-sm-4">
        <div className="right-images">
          <a><img alt="" src={formatMessage(messages.securityImage)} /></a>
        </div>
        <div className="questions">
          <h3>{formatMessage(messages.questionsHeader)}</h3>
          <p>{formatMessage(messages.questionsBody)}</p>
          <div className="que-detail">
            <a className="number">{formatMessage(messages.phoneNumber)}</a>
            <a>{formatMessage(messages.emailSupport)}</a>
            <a>{formatMessage(messages.chatLive)}</a>
          </div>
        </div>
        <div className="about-all">
          <h3>{formatMessage(messages.aboutTenantHeader)}</h3>
          <p>{formatMessage(messages.aboutTenantBody)}</p>
        </div>
        <div className="rating">
          <a><img alt="" src={formatMessage(messages.ratingImage)} /></a>
        </div>
      </div>
    );
  }
}

RightPanel.propTypes = {
  intl: intlShape.isRequired,
};

export default injectIntl(RightPanel);
