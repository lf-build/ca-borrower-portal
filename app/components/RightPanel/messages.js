/*
 * RightPanel Messages
 *
 * This contains all the text for the Faqs component.
 */
import { defineMessages } from 'react-intl';
import appSecure from 'assets/images/app-secure.png';
import arating from 'assets/images/arating.png';

export default defineMessages({
  questionsHeader: {
    id: 'app.components.RightPanel.questionsHeader',
    defaultMessage: 'Questions?',
  },
  questionsBody: {
    id: 'app.components.RightPanel.questionsBody',
    defaultMessage: 'We\'re here to answer any questions or take your application over the phone.',
  },
  phoneNumber: {
    id: 'app.components.RightPanel.phoneNumber',
    defaultMessage: '855-329-9922',
  },
  emailSupport: {
    id: 'app.components.RightPanel.emailSupport',
    defaultMessage: 'Email Support',
  },
  chatLive: {
    id: 'app.components.RightPanel.chatLive',
    defaultMessage: 'Chat Live',
  },
  aboutTenantHeader: {
    id: 'app.components.RightPanel.aboutTenantHeader',
    defaultMessage: 'About Tenant',
  },
  aboutTenantBody: {
    id: 'app.components.RightPanel.aboutTenantBody',
    defaultMessage: 'Tenant was built by the same drive and passion that fuels our clients. We offer simple business loans up to $1 million.',
  },
  securityImage: {
    id: 'app.components.RightPanel.securityImage',
    defaultMessage: appSecure,
  },
  ratingImage: {
    id: 'app.components.RightPanel.ratingImage',
    defaultMessage: arating,
  },
});
