/**
*
* Uploader
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import FileDropZone from 'components/FileDropZone';
import AlertMessage from 'components/AlertMessage';
import { IsUnderFileSizeLimit, IsValidFileFormat } from 'components/Helper/functions';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class Uploader extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { isVisible: false, showSpinner: props.showSpinner };
  }
  render() {
    const { files } = this.props.Stipulation;
    const { applicationNumber, uploadFileType, mainHeader, onDropSuccess } = this.props;
    const { formatMessage } = this.props.intl;
    const OnDropDone = (acceptedFiles) => {
      if (acceptedFiles[0].length > 12) {
        this.setState({ isError: true, message: formatMessage(messages.maxFileUploadValidation), isVisible: true });
        return;
      }
      if (!IsValidFileFormat(acceptedFiles[0])) {
        this.setState({ isError: true, message: formatMessage(messages.fileFormatValidation), isVisible: true });
        return;
      }
      if (!IsUnderFileSizeLimit(acceptedFiles[0])) {
        this.setState({ isError: true, message: formatMessage(messages.maxFileSizeValidation), isVisible: true });
        return;
      }
      const newFileObject = [];
      acceptedFiles[0].forEach((ele) => {
        newFileObject.push({ dataUrl: ele.dataUrl, file: ele.file.toLowerCase() });
      }, this);
      this.setState({ showSpinner: true });
      execute(undefined, undefined, ...'api/agreement/upload-engine'.split('/'), {
        applicationNumber,
        uploadFileType,
        fileList: newFileObject,
      })
      .then((fileResponse) => {
        sendEmail(fileResponse);
        this.setState({ isVisible: false });
        onDropSuccess().then(() => this.setState({ showSpinner: false }));
      }).catch((e) => {
        authCheck(this.props.dispatch)(e)(this.props.location.pathname);
        this.setState({ showSpinner: false, isVisible: false });
      });
    };
    const sendEmail = (fileResponse) => {
      execute(undefined, undefined, ...'api/agreement/send-doc-upload-email'.split('/'), {
        applicationNumber: this.props.applicationNumber,
        documentList: fileResponse.body,
      }).then().catch((e) => {
        authCheck(this.props.dispatch)(e)(this.props.location.pathname);
        this.setState({ showSpinner: false, isVisible: false });
      });
    };
    return (
      <div className="doc-upload-container">
        <h3>{mainHeader}</h3>
        <FileDropZone files={files} applicationNumber={applicationNumber} onDrop={OnDropDone} onDiscard={() => onDropSuccess()} uploadFileType={uploadFileType} showSpinner={this.state.showSpinner} discardDocument={this.props.discardDocument} />
        {this.state.isVisible ? (() => <span><br /><AlertMessage isError={this.state.isError} message={this.state.message} onClose={() => this.setState({ isVisible: false })} /></span>)() : <span />}
      </div>
    );
  }
}

Uploader.defaultProps = {
  showSpinner: false,
  discardDocument: false,
};

Uploader.propTypes = {
  Stipulation: React.PropTypes.any,
  applicationNumber: React.PropTypes.string,
  onDropSuccess: React.PropTypes.func.isRequired,
  uploadFileType: React.PropTypes.string.isRequired,
  mainHeader: React.PropTypes.string,
  showSpinner: React.PropTypes.bool,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
  discardDocument: React.PropTypes.bool,
  intl: intlShape.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(injectIntl(Uploader)));
