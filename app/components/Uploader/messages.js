/*
 * Uploader Messages
 *
 * This contains all the text for the Uploader component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  maxFileUploadValidation: {
    id: 'app.components.Uploader.maxFileUploadValidation',
    defaultMessage: 'Maximum 12 files can be uploaded at a time',
  },
  fileFormatValidation: {
    id: 'app.components.Uploader.fileFormatValidation',
    defaultMessage: 'Upload only PDF/JPG/PNG file',
  },
  maxFileSizeValidation: {
    id: 'app.components.Uploader.maxFileSizeValidation',
    defaultMessage: 'Maximum file size is 5MB',
  },
});
