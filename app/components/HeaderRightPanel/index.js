/**
*
* HeaderRightPanel
*
*/

import React from 'react';
import { injectIntl, intlShape } from 'react-intl';
import HeaderPhoneNumber from 'components/HeaderPhoneNumber';
import { browserHistory } from 'react-router';
import messages from './messages';

class HeaderRightPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="sign-in pull-right">
        <button type="button" className="sign-btn btn" value="Sign In" onClick={() => { browserHistory.replace('/login'); }}>{formatMessage(messages.signIn)}</button>
        <HeaderPhoneNumber />
      </div>
    );
  }
}

HeaderRightPanel.propTypes = {
  intl: intlShape.isRequired,
};

export default injectIntl(HeaderRightPanel);
