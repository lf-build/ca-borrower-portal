/*
 * HeaderRightPanel Messages
 *
 * This contains all the text for the HeaderRightPanel component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  signIn: {
    id: 'app.components.HeaderRightPanel.signIn',
    defaultMessage: 'Sign In',
  },
});
