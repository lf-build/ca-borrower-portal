/**
*
* BankLink
*
*/

import React from 'react';
import isEmpty from 'lodash/isEmpty';
import { Link, withRouter } from 'react-router';
import { connect } from 'react-redux';
import Uploader from 'components/Uploader';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import AlertMessage from 'components/AlertMessage';
import { injectIntl, intlShape } from 'react-intl';
import Spinner from 'components/Spinner';
import messages from './messages';

class BankLink extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.applicationNumber, collapse: 'in', collapseArrow: 'collapsed', notifyMessage: 'You are almost complete, please proceed to bank verification', showBankLinkLoader: false };
  }
  componentDidMount() {
    const plaid = this.props.plaid;
    window.applicationNumber = this.state.applicationNumber;
    this.handler = Plaid.create({ // eslint-disable-line
      apiVersion: plaid.apiVersion,
      clientName: plaid.clientName,
      env: plaid.env,
      key: plaid.key, // Replace with your public_key to test with live credentials
      product: plaid.product,
    // webhook: '[WEBHOOK_URL]', // Optional – use webhooks to get transaction and error updates
      selectAccount: plaid.selectAccount, // Optional – trigger the Select Account
      onLoad: () => {
      // Optional, called when Link loads
      },
      onSuccess : (publicToken, metadata)=> { // eslint-disable-line
        const payload = {};
        payload.PublicToken = metadata.public_token;
        payload.BankSupportedProductType = plaid.product[0]; // "transactions",
        payload.AccountId = metadata.account_id;
        payload.EntityId = window.applicationNumber;
        payload.EntityType = 'application';
        payload.InstitutionId = metadata.institution.institution_id;
        payload.InstitutionName = metadata.institution.name;
        execute(undefined, undefined, ...'api/account-linking/set-bank-account'.split('/'), payload)
           .then(() => {
             this.setState({ showBankLinkLoader: true });
             setTimeout(() => this.loadData(5), 5000);
           }).catch((e) => authCheck(this.props.dispatch)(e));

        // There might be a scenario when Plaid MFA (multi-factor authentication) is successful,
        // but Transaction were not pulled from Plaid side. So raising this event so that it will
        // be more helpful to get in touch with Plaid.
        this.createAccountLinkPayload(metadata);
      },
      onExit : (err, metadata) => { // eslint-disable-line
        if (err != null) {
          this.createAccountLinkPayload(metadata);
        }
      },
    });
    this.loadData();
  }
  createAccountLinkPayload(metadata) {
    const payload = {
      Institution: {},
    };
    payload.LinkSessionId = metadata.link_session_id;
    // when Plaid is successful, metadata don't have request_id and status.
    payload.LinkRequestId = metadata.request_id || undefined; // "transactions",
    payload.Status = metadata.status || undefined;
    payload.EntityId = window.applicationNumber;
    payload.EntityType = 'application';
    payload.Institution.type = metadata.institution.institution_id;
    payload.Institution.name = metadata.institution.name;

    execute(undefined, undefined, ...'api/account-linking/set-account-linking-error'.split('/'), payload)
           .then().catch((e) => authCheck(this.props.dispatch)(e));
  }
  // attempts is number of attempts before giving up

  loadData = (attempts = 1) => {
    if (attempts === 0) {
      this.setState({
        showBankLinkLoader: false,
      });
      return;
    }
    const payload = { applicationNumber: this.state.applicationNumber };
    execute(undefined, undefined, ...'api/ca/get-accounts'.split('/'), payload)
           .then((response) => {
             const isAccountsEmpty = isEmpty(response.body.accounts);
             if (!isAccountsEmpty) {
               this.setState({
                 showBankLinkLoader: false,
                 accountDetails: response.body.accounts,
                 voidedCheck: response.body.voidedCheck,
                 notifyMessage: (!isAccountsEmpty || (response.body.voidedCheck && response.body.voidedCheck.files && (response.body.voidedCheck.files.length > 2))) ? 'Your application process is now complete and our underwriters will work on your file as soon as they can.' : 'You are almost complete, please proceed to bank verification',
               });
             } else {
               setTimeout(() => this.loadData(attempts - 1), 3000);
               this.setState({
                 accountDetails: response.body.accounts,
                 voidedCheck: response.body.voidedCheck,
                 notifyMessage: (!isAccountsEmpty || (response.body.voidedCheck && response.body.voidedCheck.files && (response.body.voidedCheck.files.length > 2))) ? 'Your application process is now complete and our underwriters will work on your file as soon as they can.' : 'You are almost complete, please proceed to bank verification',
               });
             }
           }).catch((e) => {
             authCheck(this.props.dispatch)(e)(this.props.location.pathname);
             this.setState({ showBankLinkLoader: false });
           }
          );
  }
  openPlaidWidget=() => {
    this.handler.open();
  }
  toggleCollapse() {
    if (this.state.collapse === '') {
      this.setState({ collapse: 'in', collapseArrow: 'collapsed' });
    } else {
      this.setState({ collapse: '', collapseArrow: '' });
    }
  }
  render() {
    if (!this.state || !this.state.accountDetails) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    return (
      <div>
        <AlertMessage isError={false} message={this.state.notifyMessage} isHidden />
        <div className="panel panel-default">
          <div className="panel-heading">
            <h4 className="panel-title">
              <Link className={`accordion-toggle ${this.state.collapseArrow}`} data-toggle="collapse" data-parent="#accordion" onClick={() => { this.toggleCollapse(); }}>
                <div className="activity-icon bg-success small"><i className="fa fa-bank"></i></div>{formatMessage(messages.header)}</Link>
            </h4>
          </div>
          <div id="collapseTwo" className={`panel-collapse collapse ${this.state.collapse}`}>
            <div className="panel-body">
              <h4>{formatMessage(messages.connectButtonHeader)}</h4>
              <ul>
                <li>{formatMessage(messages.linkBankInfoLine1)}</li>
                <li>{formatMessage(messages.linkBankInfoLine2)}</li>
                <li>{formatMessage(messages.linkBankInfoLine3)}</li>
                <li>{formatMessage(messages.linkBankInfoLine4)}</li>
              </ul>
              { isEmpty(this.state.accountDetails) && !this.state.showBankLinkLoader &&
              <button onClick={this.openPlaidWidget} className="btn btn-default">{formatMessage(messages.connectButton)}</button>
            }{
              this.state.showBankLinkLoader && <Spinner />
            }
              { !isEmpty(this.state.accountDetails) &&
              <div>
                <h5>{formatMessage(messages.instituteName)} : {this.state.accountDetails.instituteName ? this.state.accountDetails.instituteName : formatMessage(messages.notAvailable)}</h5>
                <h5>{formatMessage(messages.accountNumber)} : {this.state.accountDetails.accountNumber ? `*******${this.state.accountDetails.accountNumber}` : formatMessage(messages.notAvailable)}</h5>
                <h5>{formatMessage(messages.accountName)} : {this.state.accountDetails.accountName && this.state.accountDetails.accountName !== '' ? this.state.accountDetails.accountName : formatMessage(messages.notAvailable)}</h5>
                <h5>{formatMessage(messages.type)} : {this.state.accountDetails.type ? this.state.accountDetails.type : formatMessage(messages.notAvailable)}</h5>
              </div>
            }
            </div>
            <div className="bankLinkingOr">
              <p>{formatMessage(messages.or)}</p>
              <h4>{formatMessage(messages.manualReview)}</h4>
              <ul>
                <li>{formatMessage(messages.manualReviewInfoLine1)}</li>
                <li>{formatMessage(messages.manualReviewInfoLine2)}</li>
                <li>{formatMessage(messages.manualReviewInfoLine3)}</li>
              </ul>
              <h4>{formatMessage(messages.uploadThreeBankStatement)}</h4>
            </div>
            <Uploader mainHeader="" Stipulation={this.state.voidedCheck} uploadFileType={'bankstatement'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />
          </div>
        </div>
      </div>
    );
  }
}

BankLink.propTypes = {
  applicationNumber: React.PropTypes.string.isRequired,
  plaid: React.PropTypes.any,
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(withRouter(injectIntl(BankLink)));
