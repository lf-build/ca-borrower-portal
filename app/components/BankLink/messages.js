/*
 * BankLink Messages
 *
 * This contains all the text for the BankLink component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.BankLink.header',
    defaultMessage: 'Bank Verification',
  },
  connectButtonHeader: {
    id: 'app.components.BankLink.connectButtonHeader',
    defaultMessage: 'Fast Processing',
  },
  connectButton: {
    id: 'app.components.BankLink.connectButton',
    defaultMessage: 'Link bank account',
  },
  accountNumber: {
    id: 'app.components.BankLink.accountNumber',
    defaultMessage: 'Account Number',
  },
  instituteName: {
    id: 'app.components.BankLink.instituteName',
    defaultMessage: 'Bank Name',
  },
  accountName: {
    id: 'app.components.BankLink.accountName',
    defaultMessage: 'Account Name',
  },
  type: {
    id: 'app.components.BankLink.type',
    defaultMessage: 'Type',
  },
  voidedCheck: {
    id: 'app.components.BankLink.voidedCheck',
    defaultMessage: 'Voided Check',
  },
  uploadThreeBankStatement: {
    id: 'app.components.BankLink.uploadThreeBankStatement',
    defaultMessage: 'Upload 3 recent months bank statements',
  },
  linkBankInfoLine1: {
    id: 'app.components.BankLink.linkBankInfoLine1',
    defaultMessage: 'Link your bank statements electronically (Recommended)',
  },
  linkBankInfoLine2: {
    id: 'app.components.BankLink.linkBankInfoLine2',
    defaultMessage: 'Your Bank will automatically send us your statements',
  },
  linkBankInfoLine3: {
    id: 'app.components.BankLink.linkBankInfoLine3',
    defaultMessage: 'Our automated underwriting system will review your statements for offers in less than 24 hours',
  },
  linkBankInfoLine4: {
    id: 'app.components.BankLink.linkBankInfoLine4',
    defaultMessage: 'We use cutting-edge encryption technology to keep your information safe and secure',
  },
  or: {
    id: 'app.components.BankLink.or',
    defaultMessage: 'OR',
  },
  manualReview: {
    id: 'app.components.BankLink.manualReview',
    defaultMessage: 'Manual Review',
  },
  manualReviewInfoLine1: {
    id: 'app.components.BankLink.manualReviewInfoLine1',
    defaultMessage: 'Upload or send us your bank statements',
  },
  manualReviewInfoLine2: {
    id: 'app.components.BankLink.manualReviewInfoLine2',
    defaultMessage: 'This process is typically slower as you have to fax or upload your documents',
  },
  manualReviewInfoLine3: {
    id: 'app.components.BankLink.manualReviewInfoLine3',
    defaultMessage: 'We then manually go through your documents and make a decision',
  },
  notAvailable: {
    id: 'app.components.BankLink.notAvailable',
    defaultMessage: 'N/A',
  },
});
