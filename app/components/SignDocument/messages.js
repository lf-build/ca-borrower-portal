/*
 * SignDocument Messages
 *
 * This contains all the text for the SignDocument component.
 */
import { defineMessages } from 'react-intl';
import DocuSign from 'assets/images/docuSign01.png';

export default defineMessages({
  header: {
    id: 'app.components.SignDocument.header',
    defaultMessage: 'Sign Documents',
  },
  resendDocuSignEmail: {
    id: 'app.components.SignDocument.resendDocuSignEmail',
    defaultMessage: 'Resend Email',
  },
  status: {
    id: 'app.components.SignDocument.status',
    defaultMessage: 'Status:',
  },
  docuSignImage: {
    id: 'app.components.SignDocument.docuSignImage',
    defaultMessage: DocuSign,
  },
});
