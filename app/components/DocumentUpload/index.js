/**
*
* DocumentUpload
*
*/

import React from 'react';
import Uploader from 'components/Uploader';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import Spinner from 'components/Spinner';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class DocumentUpload extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.applicationNumber, collapse: '', collapseArrow: '' };
  }
  componentDidMount() {
    this.loadData();
  }
  loadData() {
    return execute(undefined, undefined, ...'api/ca/todo'.split('/'), {
      applicationNumber: this.state.applicationNumber,
    })
    .then(({ body }) => {
      this.setState({ Agreement: body });
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
    });
  }
  toggleCollapse() {
    if (this.state.collapse === '') {
      this.setState({ collapse: 'in', collapseArrow: 'collapsed' });
    } else {
      this.setState({ collapse: '', collapseArrow: '' });
    }
  }
  render() {
    if (!this.state || !this.state.Agreement) {
      return <Spinner />;
    }
    const { voidedCheck, contract, photoId, taxReturn, lien, confession, proofOfProperty, merchant } = this.state.Agreement;
    const { formatMessage } = this.props.intl;
    const isSectionVisible = (merchant.isDisplay || proofOfProperty.isDisplay || voidedCheck.isDisplay || contract.isDisplay || photoId.isDisplay || taxReturn.isDisplay || lien.isDisplay || confession.isDisplay);
    if (!isSectionVisible) {
      return (<span />);
    }
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h4 className="panel-title">
            <Link className={`accordion-toggle ${this.state.collapseArrow}`} data-toggle="collapse" data-parent="#accordion" onClick={() => { this.toggleCollapse(); }}><div className="activity-icon bg-success small"><i className="fa fa-upload "></i></div> {formatMessage(messages.header)}</Link>
          </h4>
        </div>
        <div id="collapseOne" className={`panel-collapse collapse ${this.state.collapse}`}>
          <div className="panel-body">
            {voidedCheck.isDisplay && <Uploader mainHeader={formatMessage(messages.voidedCheck)} Stipulation={voidedCheck} uploadFileType={'voidedcheck'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {photoId.isDisplay && <Uploader mainHeader={formatMessage(messages.photoID)} Stipulation={photoId} uploadFileType={'photoid'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {contract.isDisplay && <Uploader mainHeader={formatMessage(messages.contract)} Stipulation={contract} uploadFileType={'signeddocument'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {taxReturn.isDisplay && <Uploader mainHeader={formatMessage(messages.taxReturn)} Stipulation={taxReturn} uploadFileType={'taxreturnstatement'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {confession.isDisplay && <Uploader mainHeader={formatMessage(messages.confession)} Stipulation={confession} uploadFileType={'confessionofjudgment'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {lien.isDisplay && <Uploader mainHeader={formatMessage(messages.lien)} Stipulation={lien} uploadFileType={'liendocument'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {proofOfProperty.isDisplay && <Uploader mainHeader={formatMessage(messages.proofOfproperty)} Stipulation={proofOfProperty} uploadFileType={'proofofpropertyownership'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
            {merchant.isDisplay && <Uploader mainHeader={formatMessage(messages.merchant)} Stipulation={merchant} uploadFileType={'merchantstatement'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} discardDocument />}
          </div>
        </div>
      </div>
    );
  }
}

DocumentUpload.propTypes = {
  applicationNumber: React.PropTypes.string.isRequired,
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(injectIntl(withRouter(DocumentUpload)));
