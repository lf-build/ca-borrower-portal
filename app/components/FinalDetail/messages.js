/*
 * FinalDetail Messages
 *
 * This contains all the text for the FinalDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  annualRevenueLabel: {
    id: 'app.components.FinalDetail.annualRevenueLabel',
    defaultMessage: 'Annual Revenue',
  },
  annualRevenuePlaceholder: {
    id: 'app.components.FinalDetail.annualRevenuePlaceholder',
    defaultMessage: 'Annual Revenue',
  },
  annualRevenueRequiredValidation: {
    id: 'app.components.FinalDetail.annualRevenueRequiredValidation',
    defaultMessage: 'Annual Revenue is required',
  },
  annualRevenueLengthValidation: {
    id: 'app.components.FinalDetail.annualRevenueLengthValidation',
    defaultMessage: 'Maximum Annual Revenue is 9999999999999999999',
  },
  averageBankBalanceLabel: {
    id: 'app.components.FinalDetail.averageBankBalanceLabel',
    defaultMessage: 'Average Bank Balance',
  },
  averageBankBalancePlaceholder: {
    id: 'app.components.FinalDetail.averageBankBalancePlaceholder',
    defaultMessage: 'Average Bank Balance',
  },
  averageBankBalanceRequiredValidation: {
    id: 'app.components.FinalDetail.averageBankBalanceRequiredValidation',
    defaultMessage: 'Average Bank Balance is required',
  },
  averageBankBalanceLengthValidation: {
    id: 'app.components.FinalDetail.averageBankBalanceLengthValidation',
    defaultMessage: 'Maximum Average Bank Balance is 9999999999999999999',
  },
  haveExistingLoanRequiredValidation: {
    id: 'app.components.FinalDetail.haveExistingLoanRequiredValidation',
    defaultMessage: 'Existing Loan Status is required',
  },
  consentsSignValidation: {
    id: 'app.components.FinalDetail.consentsSignValidation',
    defaultMessage: 'Please check consent checkbox',
  },
  consentIAgree: {
    id: 'app.components.FinalDetail.consentIAgree',
    defaultMessage: 'I agree to the ',
  },
  termsAndCondition: {
    id: 'app.components.FinalDetail.termsAndCondition',
    defaultMessage: 'Terms and Conditions',
  },
  nextButton: {
    id: 'app.components.FinalDetail.nextButton',
    defaultMessage: 'Next',
  },
  cancelButton: {
    id: 'app.components.FinalDetail.cancelButton',
    defaultMessage: 'Cancel',
  },
  haveExistingLoanLabel: {
    id: 'app.components.FinalDetail.haveExistingLoanLabel',
    defaultMessage: 'Do you have any existing business loans or cash advance?',
  },
  popupHeader: {
    id: 'app.components.FinalDetail.popupHeader',
    defaultMessage: 'Do you have any existing business loans or cash advance?',
  },
  popupBody: {
    id: 'app.components.FinalDetail.popupBody',
    defaultMessage: 'You are cancelling re-application of loan request. You will be redirected to log in page now.',
  },
  popupYesButton: {
    id: 'app.components.FinalDetail.popupYesButton',
    defaultMessage: 'OK',
  },
  popupNoButton: {
    id: 'app.components.FinalDetail.popupNoButton',
    defaultMessage: 'Cancel',
  },
});
