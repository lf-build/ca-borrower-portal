import isInRange from 'lib/validation/isInRange';
import messages from './messages';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();
  const { formatMessage } = that.props.intl;

  // Next button validations
 /* if (!values.signature) {
    errors.signature = 'Signature is required';
  } else if (!isInRange(values.signature, 2, 100)) {
    errors.signature = 'Field length must be between 2 and 100';
  }*/
  if (!values.averageBankBalance) {
    errors.averageBankBalance = formatMessage(messages.averageBankBalanceRequiredValidation);
  } else if (!isInRange(isNaN(values.averageBankBalance) ? parseInt(values.averageBankBalance.replace(/[$, ]/g, ''), 10) : values.averageBankBalance, 1, 9999999999999999999)) {
    errors.averageBankBalance = formatMessage(messages.averageBankBalanceLengthValidation);
  }
  if (!values.annualRevenue) {
    errors.annualRevenue = formatMessage(messages.annualRevenueRequiredValidation);
  } else if (!isInRange(isNaN(values.annualRevenue) ? parseInt(values.annualRevenue.replace(/[$, ]/g, ''), 10) : values.annualRevenue, 1, 9999999999999999999)) {
    errors.annualRevenue = formatMessage(messages.annualRevenueLengthValidation);
  }

  if (!values.haveExistingLoan) {
    errors.haveExistingLoan = formatMessage(messages.haveExistingLoanRequiredValidation);
  }
  if (!values.consents) {
    errors.consents = formatMessage(messages.consentsSignValidation);
  }

  return errors;
};

export default validateForm;
