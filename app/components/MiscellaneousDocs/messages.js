/*
 * MiscellaneousDocs Messages
 *
 * This contains all the text for the MiscellaneousDocs component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.MiscellaneousDocs.header',
    defaultMessage: 'Miscellaneous Documents',
  },
});
