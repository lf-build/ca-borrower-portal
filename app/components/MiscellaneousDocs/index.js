/**
*
* MiscellaneousDocs
*
*/

import React from 'react';
import Uploader from 'components/Uploader';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import Spinner from 'components/Spinner';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class MiscellaneousDocs extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { applicationNumber: this.props.applicationNumber, collapse: '', collapseArrow: '' };
  }
  componentDidMount() {
    this.loadData();
  }
  loadData() {
    return execute(undefined, undefined, ...'api/application-document/get-category-docs-list'.split('/'), {
      applicationNumber: this.state.applicationNumber,
      category: 'other',
    })
    .then(({ body }) => {
      const fileList = [];
      body.forEach((ele) => {
        fileList.push({ fileName: ele.fileName, fileId: ele.id });
      }, this);
      const finalList = { files: fileList };
      this.setState({ Files: finalList });
    }).catch((e) => {
      authCheck(this.props.dispatch)(e)(this.props.location.pathname);
    });
  }
  toggleCollapse() {
    if (this.state.collapse === '') {
      this.setState({ collapse: 'in', collapseArrow: 'collapsed' });
    } else {
      this.setState({ collapse: '', collapseArrow: '' });
    }
  }
  render() {
    if (!this.state || !this.state.Files) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    return (
      <div className="panel panel-default">
        <div className="panel-heading">
          <h4 className="panel-title">
            <Link className={`accordion-toggle ${this.state.collapseArrow}`} data-toggle="collapse" data-parent="#accordion" onClick={() => { this.toggleCollapse(); }}><div className="activity-icon bg-success small"><i className="fa fa-upload "></i></div> {formatMessage(messages.header)}</Link>
          </h4>
        </div>
        <div id="collapseOne" className={`panel-collapse collapse ${this.state.collapse}`}>
          <div className="panel-body">
            <Uploader mainHeader="" Stipulation={this.state.Files} uploadFileType={'other'} applicationNumber={this.state.applicationNumber} onDropSuccess={() => this.loadData()} showSpinner={false} />
          </div>
        </div>
      </div>
    );
  }
}

MiscellaneousDocs.propTypes = {
  applicationNumber: React.PropTypes.string.isRequired,
  intl: intlShape.isRequired,
  dispatch: React.PropTypes.func.isRequired,
  location: React.PropTypes.object.isRequired,
};

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapDispatchToProps)(injectIntl(withRouter(MiscellaneousDocs)));
