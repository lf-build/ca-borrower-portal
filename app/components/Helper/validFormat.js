export const defaultValidFileFormat = ['image/png', 'image/jpeg', 'application/pdf'];

export const defaultValidImageFormat = ['image/png', 'image/jpeg'];

export const defaultMaxFileSize = 5000000; // in bytes

export const plaidErrorCodes = ['INVALID_CREDENTIALS', 'INVALID_MFA'];
