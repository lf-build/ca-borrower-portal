/**
*
* RequestDrawdown
*
*/

import React from 'react';
// import Spinner from 'components/Spinner';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { createStructuredSelector } from 'reselect';
import MoneyField from '@ui/MoneyField';
import Form from '@ui/Form';
import makeAuthSelector from 'sagas/auth/selectors';
import Spinner from 'components/Spinner';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';
import validateForm from './validate';

class RequestDrawdown extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { drawAmount: undefined, showSpinner: false };
  }
  afterSubmit=(err, data) => {
    if (data) {
      this.props.onDrawSuccess();
    }
    this.setState({ showSpinner: false });
  }
  modifyJson = (values) => {
    let drawAmount = 0;
    if (values && values.drawAmount) {
      if (isNaN(values.drawAmount)) {
        if (values.drawAmount.indexOf('$') !== -1 || values.drawAmount.indexOf(',') !== -1) {
          drawAmount = parseInt(values.drawAmount.replace(/[$, ]/g, ''), 10);
        }
      } else {
        drawAmount = values.drawAmount;
      }
    }
    const payload = {
      requestedAmount: drawAmount,
      applicationNumber: this.props.applicationNumber };
    this.setState({ showSpinner: true });
    return payload;
  };
  initialValues = () => {
    const result = { drawAmount: this.state.drawAmount };
    return result;
  };
  handleOnLoadError = () => {
    // do not add authCheck here
  };
  render() {
    const { formatMessage } = this.props.intl;
    return (
      <div className="col-sm-12 login-box">
        <div className="panel-group todo-panel-group" id="accordion">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="panel-title">
                <a className="accordion-toggle">{formatMessage(messages.header)}<span ></span></a>
              </h4>
            </div>
            <div id="collapseOne" className="panel-collapse collapse in" >
              <div className="panel-body">
                <p>{formatMessage(messages.amount)}</p>
                <div className="draw-box">
                  <div className="draw-before">
                    <Form
                      afterSubmit={(err, data) => this.afterSubmit(err, data)}
                      name="drawRequest"
                      validate={(values) => validateForm(this, values)}
                      action="api/drawdown/request"
                      payloadBuilder={(values) => this.modifyJson(values)}
                      initialValuesBuilder={(value) => this.initialValues(value)}
                    >
                      <div className="col-md-4 col-sm-4">
                        <MoneyField className="form-control" name="drawAmount" disabled={this.state.isVerified} />
                      </div>
                      <input className="btn submit-btn" value={formatMessage(messages.drawButton)} type="submit" />
                    </Form>
                    {this.state.showSpinner && <Spinner />}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

RequestDrawdown.propTypes = {
  applicationNumber: React.PropTypes.any,
  onDrawSuccess: React.PropTypes.func,
  intl: intlShape.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});


function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(RequestDrawdown)));

