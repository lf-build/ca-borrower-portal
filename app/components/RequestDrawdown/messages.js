/*
 * RequestDrawdown Messages
 *
 * This contains all the text for the RequestDrawdown component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.RequestDrawdown.header',
    defaultMessage: 'Make A Draw',
  },
  amount: {
    id: 'app.components.RequestDrawdown.amount',
    defaultMessage: 'Type the amount you would like to withdraw',
  },
  amountRequiredValidation: {
    id: 'app.components.RequestDrawdown.amountRequiredValidation',
    defaultMessage: 'Draw amount is required',
  },
  amountGreatherThanZeroValidation: {
    id: 'app.components.RequestDrawdown.amountGreatherThanZeroValidation',
    defaultMessage: 'Draw amount must be greater than 0',
  },
  drawButton: {
    id: 'app.components.RequestDrawdown.drawButton',
    defaultMessage: 'Draw',
  },
});
