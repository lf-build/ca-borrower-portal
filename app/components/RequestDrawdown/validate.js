import messages from './messages';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();
  const { formatMessage } = that.props.intl;
  // Next button validations
  if (!values.drawAmount) {
    errors.drawAmount = formatMessage(messages.amountRequiredValidation);
  } else if (parseInt(values.drawAmount.replace(/[$, ]/g, ''), 10) === 0) {
    errors.drawAmount = formatMessage(messages.amountGreatherThanZeroValidation);
  }
  return errors;
};

export default validateForm;
