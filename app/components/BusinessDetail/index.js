/**
*
* BusinessDetail
*
*/

import React from 'react';
import lodash from 'lodash';
import { Field, Fields, reset } from 'redux-form/immutable';
import Form from '@ui/Form';
import TextField from '@ui/TextField';
import Spinner from 'components/Spinner';
import { ein, canadaZipCode,usaZipCode } from 'lib/masks';
import decodeEntity from 'lib/decodeEntity';
import DateField from '@ui/DateField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';
import validateForm from './validate';

const DropDownOptions = (obj) => (
  <option key={obj.key} value={obj.key}>{decodeEntity(obj.value)}</option>
);

const DropDownList = ({ input, label, options, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className=" form-group">
    <label htmlFor="label3">{label}</label>
    <select className="select_fields" {...input} placeholder={label} type={type}>
      <option key="0"></option>
      {options && Object.entries(options).map(([key, value]) => ({ key, value })).map(DropDownOptions)}
    </select>
    {touched &&
      ((error && <span className="error_message">{error}</span>) ||
        (warning && <span>{warning}</span>))}
  </div>
);

const ButtonPanel = (props) => {
  const { submitting,showSpinner ,resetForm, intl} = props; // eslint-disable-line
  return (
    <div className="form-btn pull-right">
      {submitting && <Spinner />}
      <input type="button" disabled={showSpinner} onClick={() => resetForm()} className="btn submit-btn" value={intl.formatMessage(messages.cancelButton)} />
      <span>&nbsp;</span>
      <input type="submit" disabled={showSpinner} className="btn submit-btn" value={intl.formatMessage(messages.nextButton)} />
    </div>
  );
};

const renderCountryState = (fields) => {
  const parsedMessages = localityNormalizer(messages);
  const { country, state, parentProps } = fields;
  const { intl } = parentProps;
  const selectedCountry = country.input.value;
  // If country is usa then states will contain US states otherwise it will contain canada states.
  const states = selectedCountry === "usa" ? parentProps.lookup.State : parentProps.lookup.CanadaStates;

  return <div>
    <div className="col-md-4 col-sm-4">
      <DropDownList label={intl.formatMessage(messages.countryLabel)} {...country} options={parentProps.lookup.Country} type="select" />
    </div>
    <div className="col-md-4 col-sm-4">
      <DropDownList label={intl.formatMessage(messages.stateLabel)} {...state} options={states} type="select" />
    </div>
    <div className="col-md-4 col-sm-4">
      <TextField className="form-control" name="city" {...parsedMessages.city} />
    </div>
    <div className="col-md-4 col-sm-4">
      <TextField className="form-control" name="postalCode" mask={selectedCountry === 'usa' ? usaZipCode : canadaZipCode} {...parsedMessages.postalCode} />
    </div>
  </div>
}

class BusinessDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { showSpinner: false };
  }
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(this.props.payloadGenerator(finalValues, 'business'), lodash.identity);
    if (payload.BusinessTaxID) {
      payload.BusinessTaxID = payload.BusinessTaxID.replace('-', '');
    }
    payload.ClientIpAddress = this.props.ip;
    this.setState({ showSpinner: true });
    return payload;
  };

  afterSubmit = (err, data) => {
    this.setState({ showSpinner: false });
    this.props.updateStateAfterSubmit(err, data, 'business');
  }
  initialValues = () => {
    const returnValue = {};
    if (this.props.business) {
      return this.props.business;
    }
    return returnValue;
  };
  handleOnLoadError = () => {
    // do not add authCheck here
  };
  resetForm = () => {
    this.props.dispatch(reset('businessDetails'));
  };

  render() {
    const parsedMessages = localityNormalizer(messages);
    const { formatMessage } = this.props.intl;
    return (
      <section id="content2" className="tab-section">
        <Form
          passPropsTo="ButtonPanel"
          afterSubmit={(err, data) => this.afterSubmit(err, data)}
          initialValuesBuilder={() => this.initialValues()}
          payloadBuilder={(values) => this.modifyJson(values)}
          validate={(values) => validateForm(this, values)}
          name="businessDetails"
          action="api/ca/save-lead"
          onLoadError={this.handleOnLoadError}
        >
          <div className="comman_application_wraper">
            <div className="business-details">
              <div className="col-md-4 col-sm-4">
                <TextField className="form-control" name="legalCompanyName" {...parsedMessages.legalCompanyName} />
              </div>
              <div className="col-md-4 col-sm-4">
                <TextField className="form-control" name="dba" {...parsedMessages.dba} />
              </div>
              <div className="col-md-4 col-sm-4">
                <TextField className="form-control" name="businessAddress" {...parsedMessages.businessAddress} />
              </div>
              <Fields names={['country', 'state', 'city', 'postalCode']} parentProps={this.props} component={renderCountryState} />
              <div className="col-md-4 col-sm-4">
                <div className=" form-group">
                  <Field name="homeOwnerType" label={formatMessage(messages.homeOwnerTypeLabel)} options={this.props.lookup.PropertyType} component={DropDownList} type="select" />
                </div>
              </div>
              <div className="col-md-4 col-sm-4">
                <div className=" form-group">
                  <Field name="industry" label={formatMessage(messages.industryLabel)} options={this.props.lookup.Industry} component={DropDownList} type="select" />
                </div>
              </div>
              {/* <div className="col-md-4 col-sm-4">
                <TextField mask={sic} className="form-control" name="sicCode" {...parsedMessages.sicCode} />
              </div> */}
              <div className="col-md-4 col-sm-4">
                <TextField name="businessTaxId" disabled={this.props.reapplyDisableFields} mask={ein} className="form-control" {...parsedMessages.businessTaxId} />
              </div>
              <div className="col-md-4 col-sm-4">
                <div className=" form-group">
                  <Field name="entityType" label={formatMessage(messages.entityTypeLabel)} options={this.props.lookup.BusinessTypes} component={DropDownList} type="select" />
                </div>
              </div>
              {/* <div className="col-md-4 col-sm-4">
                <div className=" form-group">
                  <Field label="Address Type" name="addressType" options={this.props.lookup.AddressType} component={DropDownList} type="select" />
                </div>
              </div>*/}
              <div className="col-md-4 col-sm-4">
                <DateField
                  sectionClassName="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad"
                  dateFormat="mm/dd/yyyy"
                  name="dateEstablished"
                  maxYears={100}
                  {...parsedMessages.dateOfBirth}
                />
              </div>
            </div>
            <div className="comman_applicationinner business-details">
              <div className="col-md-4 col-sm-4">{this.state.showSpinner && <Spinner />}</div>
              <ButtonPanel resetForm={this.resetForm} showSpinner={this.state.showSpinner} intl={this.props.intl} />
            </div>
          </div>
        </Form>
      </section>
    );
  }
}

BusinessDetail.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  payloadGenerator: React.PropTypes.any,
  business: React.PropTypes.any,
  lookup: React.PropTypes.any,
  updateStateAfterSubmit: React.PropTypes.any,
  ip: React.PropTypes.string,
  reapplyDisableFields: React.PropTypes.bool,
  intl: intlShape.isRequired,
};

export default injectIntl(BusinessDetail);
