import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import { postalCodeRegex, canadaZipCodeRegex, dateRegex, textRegex } from 'lib/validation/regexList';
import messages from './messages';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();
  const { formatMessage } = that.props.intl;

  // Next button validations
  if (!values.legalCompanyName) {
    errors.legalCompanyName = formatMessage(messages.legalCompanyNameRequiredValidation);
  } else if (!isInRange(values.legalCompanyName, 2, 100)) {
    errors.legalCompanyName = formatMessage(messages.legalCompanyNameLengthValidation);
  }
  if (!values.businessAddress) {
    errors.businessAddress = formatMessage(messages.businessAddressRequiredValidation);
  } else if (!isInRange(values.businessAddress, 2, 100)) {
    errors.businessAddress = formatMessage(messages.businessAddressLengthValidation);
  }
  if (!values.businessTaxId) {
    errors.businessTaxId = formatMessage(messages.businessTaxIdRequiredValidation);
  } else if (!isInRange(values.businessTaxId.replace('-', ''), 9, 9)) {
    errors.businessTaxId = formatMessage(messages.businessTaxIdLengthValidation);
  }
 /*  if (!values.sicCode) {
    errors.sicCode = 'SIC Code is required';
  } else if (!isInRange(values.sicCode, 4, 4)) {
    errors.sicCode = 'Field length must be 4';
  } */
  if (!values.city) {
    errors.city = formatMessage(messages.cityRequiredValidation);
  } else if (!isInRange(values.city, 2, 100)) {
    errors.city = formatMessage(messages.cityLengthValidation);
  } else if (!isRegexMatch(values.city.trim(), textRegex)) {
    errors.city = formatMessage(messages.cityNumberSpecialCharacterValidation);
  }
  if (!values.state) {
    errors.state = formatMessage(messages.stateRequiredValidation);
  }
  if (!values.country) {
    errors.country = formatMessage(messages.countryRequiredValidation);
  }
  if (!values.postalCode) {
    errors.postalCode = formatMessage(messages.postalCodeRequiredValidation);
  } else if (!isRegexMatch(values.postalCode, values.country === "usa" ? postalCodeRegex : canadaZipCodeRegex)) {
    errors.postalCode = formatMessage(messages.postalCodeInvalid);
  }
  if (!values.dateEstablished) {
    errors.dateEstablished = formatMessage(messages.dateOfBirthRequiredValidation);
  } else if (!isRegexMatch(values.dateEstablished, dateRegex)) {
    errors.dateEstablished = formatMessage(messages.dateOfBirthInvalid);
  } else {
    const parts = values.dateEstablished.split('/');
    const mydate = new Date(parts[2], parts[0] - 1, parts[1]);
    if (mydate >= new Date()) {
      errors.dateEstablished = formatMessage(messages.dateOfBirthFutureDateInvalid);
    }
  }

  /* if (!values.addressType) {
    errors.addressType = 'Address type is required';
  }*/

  if (!values.entityType) {
    errors.entityType = formatMessage(messages.entityTypeRequiredValidation);
  }

  if (!values.industry) {
    errors.industry = formatMessage(messages.industryRequiredValidation);
  }

  if (!values.homeOwnerType) {
    errors.homeOwnerType = formatMessage(messages.homeOwnerTypeRequiredValidation);
  }

  return errors;
};

export default validateForm;
