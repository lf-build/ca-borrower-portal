/*
 * BusinessDetail Messages
 *
 * This contains all the text for the BusinessDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  legalCompanyNameLabel: {
    id: 'app.components.BusinessDetail.legalCompanyNameLabel',
    defaultMessage: 'Legal Company Name',
  },
  legalCompanyNamePlaceholder: {
    id: 'app.components.BusinessDetail.legalCompanyNamePlaceholder',
    defaultMessage: 'Legal Company Name',
  },
  legalCompanyNameRequiredValidation: {
    id: 'app.components.BusinessDetail.legalCompanyNameRequiredValidation',
    defaultMessage: 'Legal Business Name is required',
  },
  legalCompanyNameLengthValidation: {
    id: 'app.components.BusinessDetail.legalCompanyNameLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  businessAddressLabel: {
    id: 'app.components.BusinessDetail.businessAddressLabel',
    defaultMessage: 'Business Address',
  },
  businessAddressPlaceholder: {
    id: 'app.components.BusinessDetail.businessAddressPlaceholder',
    defaultMessage: 'Business Address',
  },
  businessAddressRequiredValidation: {
    id: 'app.components.BusinessDetail.businessAddressRequiredValidation',
    defaultMessage: 'Business Address is required',
  },
  businessAddressLengthValidation: {
    id: 'app.components.BusinessDetail.businessAddressLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  dbaLabel: {
    id: 'app.components.BusinessDetail.dbaLabel',
    defaultMessage: 'DBA',
  },
  dbaPlaceholder: {
    id: 'app.components.BusinessDetail.dbaPlaceholder',
    defaultMessage: 'DBA',
  },
  cityLabel: {
    id: 'app.components.BusinessDetail.cityLabel',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.BusinessDetail.cityPlaceholder',
    defaultMessage: 'City',
  },
  cityRequiredValidation: {
    id: 'app.components.BusinessDetail.cityRequiredValidation',
    defaultMessage: 'City is required',
  },
  cityLengthValidation: {
    id: 'app.components.BusinessDetail.cityLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  cityNumberSpecialCharacterValidation: {
    id: 'app.components.BusinessDetail.cityNumberSpecialCharacterValidation',
    defaultMessage: 'Numbers or special characters not allowed',
  },
  stateLabel: {
    id: 'app.components.BusinessDetail.stateLabel',
    defaultMessage: 'State',
  },
  statePlaceholder: {
    id: 'app.components.BusinessDetail.statePlaceholder',
    defaultMessage: 'State',
  },
  stateRequiredValidation: {
    id: 'app.components.BusinessDetail.stateRequiredValidation',
    defaultMessage: 'State is required',
  },
  countryLabel: {
    id: 'app.components.BusinessDetail.countryLabel',
    defaultMessage: 'Country',
  },
  countryPlaceholder: {
    id: 'app.components.BusinessDetail.countryPlaceholder',
    defaultMessage: 'Country',
  },
  countryRequiredValidation: {
    id: 'app.components.BusinessDetail.countryRequiredValidation',
    defaultMessage: 'Country is required',
  },
  postalCodeLabel: {
    id: 'app.components.BusinessDetail.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  postalCodePlaceholder: {
    id: 'app.components.BusinessDetail.postalCodePlaceholder',
    defaultMessage: 'Postal Code',
  },
  postalCodeRequiredValidation: {
    id: 'app.components.BusinessDetail.postalCodeRequiredValidation',
    defaultMessage: 'Postal Code is required',
  },
  postalCodeInvalid: {
    id: 'app.components.BusinessDetail.postalCodeInvalid',
    defaultMessage: 'Invalid Postal Code',
  },
  businessTaxIdLabel: {
    id: 'app.components.BusinessDetail.businessTaxIdLabel',
    defaultMessage: 'Business Tax ID',
  },
  businessTaxIdPlaceholder: {
    id: 'app.components.BusinessDetail.businessTaxIdPlaceholder',
    defaultMessage: 'Business Tax ID',
  },
  businessTaxIdRequiredValidation: {
    id: 'app.components.BusinessDetail.businessTaxIdRequiredValidation',
    defaultMessage: 'Business Tax ID is required',
  },
  businessTaxIdLengthValidation: {
    id: 'app.components.BusinessDetail.businessTaxIdLengthValidation',
    defaultMessage: 'Field length must be 9',
  },
/*   sicCodeLabel: {
    id: 'app.components.BusinessDetail.sicCodeLabel',
    defaultMessage: 'SIC Code',
  },
  sicCodePlaceholder: {
    id: 'app.components.BusinessDetail.sicCodePlaceholder',
    defaultMessage: 'SIC Code',
  }, */
  dateOfBirthLabel: {
    id: 'app.components.BusinessDetail.dateOfBirthLabel',
    defaultMessage: 'Date Established',
  },
  dateOfBirthRequiredValidation: {
    id: 'app.components.BusinessDetail.dateOfBirthRequiredValidation',
    defaultMessage: 'Date established is required',
  },
  dateOfBirthInvalid: {
    id: 'app.components.BusinessDetail.dateOfBirthInvalid',
    defaultMessage: 'Invalid Date',
  },
  dateOfBirthFutureDateInvalid: {
    id: 'app.components.BusinessDetail.dateOfBirthFutureDateInvalid',
    defaultMessage: 'Date established can not be a future date',
  },
  homeOwnerTypeLabel: {
    id: 'app.components.BusinessDetail.homeOwnerTypeLabel',
    defaultMessage: 'Rent Or Own this location?',
  },
  homeOwnerTypeRequiredValidation: {
    id: 'app.components.BusinessDetail.homeOwnerTypeRequiredValidation',
    defaultMessage: 'Property type is required',
  },
  industryLabel: {
    id: 'app.components.BusinessDetail.industryLabel',
    defaultMessage: 'Industry',
  },
  industryRequiredValidation: {
    id: 'app.components.BusinessDetail.industryRequiredValidation',
    defaultMessage: 'Industry is required',
  },
  entityTypeLabel: {
    id: 'app.components.BusinessDetail.entityTypeLabel',
    defaultMessage: 'Entity Type',
  },
  entityTypeRequiredValidation: {
    id: 'app.components.BusinessDetail.entityTypeRequiredValidation',
    defaultMessage: 'Entity type is required',
  },
  nextButton: {
    id: 'app.components.BusinessDetail.nextButton',
    defaultMessage: 'Next',
  },
  cancelButton: {
    id: 'app.components.BusinessDetail.cancelButton',
    defaultMessage: 'Cancel',
  },
});
