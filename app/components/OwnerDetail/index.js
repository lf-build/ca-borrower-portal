/**
*
* OwnerDetail
*
*/

import React from 'react';
import lodash from 'lodash';
import makeAuthSelector from 'sagas/auth/selectors';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { FieldArray, reset, Fields } from 'redux-form/immutable';
import decodeEntity from 'lib/decodeEntity';
import { canadaZipCode,usaZipCode } from 'lib/masks';
import Form from '@ui/Form';
import Spinner from 'components/Spinner';
import TextField from '@ui/TextField';
import PhoneField from '@ui/PhoneField';
import SSNField from '@ui/SSNField';
import DateField from '@ui/DateField';
import localityNormalizer from '@ui/Utils/localityNormalizer';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';
import validateForm from './validate';

const parsedMessages = localityNormalizer(messages);

const DropDownOptions = (obj) => (
  <option key={obj.key} value={obj.key}>{decodeEntity(obj.value)}</option>
  );

const DropDownList = ({ input, label, options, type, meta: { touched, error, warning } }) => ( // eslint-disable-line
  <div className=" form-group">
    <label htmlFor="label3">{label}</label>
    <select className="select_fields" {...input} placeholder={label} type={type}>
      <option key="0"></option>
      {options && Object.entries(options).map(([key, value]) => ({ key, value })).map(DropDownOptions)}
    </select>
    {touched &&
        ((error && <span className="error_message">{error}</span>) ||
          (warning && <span>{warning}</span>))}
  </div>
);
const ButtonPanel = (props) => {
  const { submitting, showSpinner ,resetForm, intl } = props; // eslint-disable-line
  return (
    <div className="form-btn pull-right">
      {submitting && <Spinner />}
      <input type="button" disabled={showSpinner} onClick={() => resetForm()} className="btn submit-btn" value={intl.formatMessage(messages.cancelButton)} />
      <span>&nbsp;</span>
      <input type="submit" disabled={showSpinner} className="btn submit-btn" value={intl.formatMessage(messages.nextButton)} />
    </div>
  );
};

const renderCountryState = (fields) => {
  const { parentProps,owners,ownerIndex } = fields;
  const { intl } = parentProps;
  const parsedMessages = localityNormalizer(messages);
  const selectedCountry = owners[ownerIndex].country.input.value;
  // If country is usa then states will contain US states otherwise it will contain canada states.
  const states = selectedCountry === "usa" ? parentProps.lookup.State : parentProps.lookup.CanadaStates;
  return <div>
  <div className="col-md-4 col-sm-4">
   <DropDownList label={intl.formatMessage(messages.countryLabel)} {...owners[ownerIndex].country} options={parentProps.lookup.Country} type="select" />
  </div> 
  <div className="col-md-4 col-sm-4">
  <DropDownList label={intl.formatMessage(messages.stateLabel)} {...owners[ownerIndex].state} options={ states } type="select" />
  </div>
    <div className="col-md-4 col-sm-4">
      <TextField className="form-control" {...owners[ownerIndex].city} name={`${owners[ownerIndex]}.city`} {...parsedMessages.city} />
    </div>
    <div className="col-md-4 col-sm-4">
      <TextField className="form-control" {...owners[ownerIndex].postalCode} name={`${owners[ownerIndex]}.postalCode`} mask={selectedCountry === 'usa' ? usaZipCode : canadaZipCode} {...parsedMessages.postalCode} />
    </div>
</div>
}

class OwnerDetail extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0, isDisplay: true, showSpinner: false };
  }

  RenderOwner = ({ fields, activeIndex, user }) => (
    <div> <div className="form-btn ">
      {
        fields.length === (user && user.maxOwnersCount) ? null :
          <input type="button" onClick={() => fields.push({})} className="btn submit-btn add-another" value={this.props.intl.formatMessage(messages.addAnotherOwnerButton)} />
      }
    </div>
      {fields.map((owner, index) =>
        <div key={index} className={activeIndex === index ? '' : ''}>
          <div className="form-btn border-top">
            <h4 className="heading-text">{this.props.intl.formatMessage(messages.ownerHeading)} {index + 1}</h4>
            {index === 0 ? null :
              <input onClick={() => fields.remove(index)} className="btn submit-btn add-another" value={this.props.intl.formatMessage(messages.removeButton)} type="button" />
            }
          </div>
          <div className="comman_applicationinner business-details ">
            <div className="col-md-4 col-sm-4">
              <TextField className="form-control" name={`${owner}.firstName`} {...parsedMessages.firstName} />
            </div>
            <div className="col-md-4 col-sm-4">
              <TextField className="form-control" name={`${owner}.lastName`} {...parsedMessages.lastName} />
            </div>
            <div className="col-md-4 col-sm-4">
              <TextField className="form-control" name={`${owner}.homeAddress`} {...parsedMessages.homeAddress} />
            </div>
            <Fields names={[`${owner}.country`, `${owner}.state`, `${owner}.city`, `${owner}.postalCode`]} parentProps={this.props} ownerIndex={index} component={renderCountryState} />
            <div className="col-md-4 col-sm-4">
              <PhoneField className="form-control" name={`${owner}.mobilePhone`} {...parsedMessages.mobilePhone} />
            </div>
            <div className="col-md-4 col-sm-4">
              <TextField className="form-control" name={`${owner}.ownership`} {...parsedMessages.ownership} />
            </div>
            <div className="col-md-4 col-sm-4">
              <SSNField disabled={this.props.reapplyDisableFields} className="form-control" name={`${owner}.ssn`} {...parsedMessages.ssn} />
            </div>
            <div className="col-md-4 col-sm-4">
              <TextField className="form-control" name={`${owner}.emailAddress`} {...parsedMessages.emailAddress} />
            </div>
            <div className="col-md-4 col-sm-4">
              <DateField
                sectionClassName="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-pad"
                dateFormat="mm/dd/yyyy"
                name={`${owner}.dateOfBirth`}
                maxYears={100}
                {...parsedMessages.dateOfBirth}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
  modifyJson = (values) => {
    const finalValues = {
      ...values,
    };
    finalValues.owners = values.owners.map((owner) => {
      const finalOwner = owner;
      if (owner && owner.ssn) {
        finalOwner.ssn = owner.ssn.replace(/-/g, '');
      }
      if (owner && owner.mobilePhone) {
        finalOwner.mobilePhone = owner.mobilePhone.replace('(', '').replace(')', '').replace(/-/g, '').replace(/ /g, '');
      }
      return finalOwner;
    });
    // Remove null, undefined from payload object
    const payload = lodash.pickBy(this.props.payloadGenerator(finalValues, 'owner'), lodash.identity);
    const owners = payload.Owners.map((item) => lodash.pickBy(item, lodash.identity));
    // delete payload.owners;
    payload.Owners = owners;
    if (payload.BusinessTaxID) {
      payload.BusinessTaxID = payload.BusinessTaxID.replace('-', '');
    }
    payload.ClientIpAddress = this.props.ip;
    this.state.showSpinner = true;
    this.setState(this.state);
    return payload;
  };

  afterSubmit = (err, data) => {
    this.state.showSpinner = false;
    this.setState(this.state);
    this.props.updateStateAfterSubmit(err, data, 'owner');
  };
  initialValues = () => {
    const returnValue = { owners: [{}] };
    if (this.props.owner && this.props.owner.owners) {
      return this.props.owner;
    }
    return returnValue;
  };
  handleOnLoadError = () => {
    // do not add authCheck here
  };
  resetForm = () => {
    this.props.dispatch(reset('ownerDetails'));
  };
  render() {
    const { auth: { user } } = this.props;
    return (
      <section id="content3" className="tab-section">
        <Form
          passPropsTo="ButtonPanel"
          afterSubmit={(err, data) => this.afterSubmit(err, data)}
          initialValuesBuilder={(value) => this.initialValues(value)}
          payloadBuilder={(values) => this.modifyJson(values)}
          validate={(values) => validateForm(this, values)}
          name="ownerDetails"
          action="api/ca/save-lead"
          onLoadError={this.handleOnLoadError}
        >
          <div className="comman_application_wraper">
            <FieldArray addMember={this.addMember} isDisplay={this.state.isDisplay} removeMember={this.removeMember} activeIndex={this.state.activeIndex} tabClick={this.tabClick} name="owners" component={this.RenderOwner} user={user} />
            <div className="comman_applicationinner business-details">
              <div className="col-md-4 col-sm-4">{this.state.showSpinner && <Spinner />}</div>
              <ButtonPanel resetForm={this.resetForm} showSpinner={this.state.showSpinner} intl={this.props.intl} />
            </div>
          </div>
        </Form>
      </section>
    );
  }
}

OwnerDetail.propTypes = {
  dispatch: React.PropTypes.func.isRequired,
  payloadGenerator: React.PropTypes.any,
  lookup: React.PropTypes.any,
  owner: React.PropTypes.any,
  updateStateAfterSubmit: React.PropTypes.any,
  ip: React.PropTypes.string,
  reapplyDisableFields: React.PropTypes.bool,
  auth: React.PropTypes.object,
  intl: intlShape.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(OwnerDetail));
