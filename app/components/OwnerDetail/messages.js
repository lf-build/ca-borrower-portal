/*
 * OwnerDetail Messages
 *
 * This contains all the text for the OwnerDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  firstNameLabel: {
    id: 'app.components.OwnerDetail.firstNameLabel',
    defaultMessage: 'First Name',
  },
  firstNamePlaceholder: {
    id: 'app.components.OwnerDetail.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  firstNameRequiredValidation: {
    id: 'app.components.OwnerDetail.firstNameRequiredValidation',
    defaultMessage: 'First Name is required',
  },
  firstNameLengthValidation: {
    id: 'app.components.OwnerDetail.firstNameLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  firstNameNumberSpecialCharacterValidation: {
    id: 'app.components.OwnerDetail.firstNameNumberSpecialCharacterValidation',
    defaultMessage: 'Numbers or special characters not allowed',
  },
  lastNameLabel: {
    id: 'app.components.OwnerDetail.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.OwnerDetail.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  lastNameRequiredValidation: {
    id: 'app.components.OwnerDetail.lastNameRequiredValidation',
    defaultMessage: 'Last Name is required',
  },
  lastNameLengthValidation: {
    id: 'app.components.OwnerDetail.lastNameLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  lastNameNumberSpecialCharacterValidation: {
    id: 'app.components.OwnerDetail.lastNameNumberSpecialCharacterValidation',
    defaultMessage: 'Numbers or special characters not allowed',
  },
  mobilePhoneLabel: {
    id: 'app.components.OwnerDetail.mobilePhoneLabel',
    defaultMessage: 'Mobile Phone',
  },
  mobilePhonePlaceholder: {
    id: 'app.components.OwnerDetail.mobilePlaceholder',
    defaultMessage: 'Mobile Phone #',
  },
  mobilePhoneRequiredValidation: {
    id: 'app.components.OwnerDetail.mobilePhoneRequiredValidation',
    defaultMessage: 'Mobile Phone is required',
  },
  mobilePhoneInvalid: {
    id: 'app.components.OwnerDetail.mobilePhoneInvalid',
    defaultMessage: 'Invalid Primary Phone',
  },
  homeAddressLabel: {
    id: 'app.components.OwnerDetail.homeAddressLabel',
    defaultMessage: 'Home Address',
  },
  homeAddressPlaceholder: {
    id: 'app.components.OwnerDetail.homeAddressPlaceholder',
    defaultMessage: 'Home Address',
  },
  homeAddressRequiredValidation: {
    id: 'app.components.OwnerDetail.homeAddressRequiredValidation',
    defaultMessage: 'Home address is required',
  },
  homeAddressLengthValidation: {
    id: 'app.components.OwnerDetail.homeAddressLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  emailAddressLabel: {
    id: 'app.components.OwnerDetail.emailAddressLabel',
    defaultMessage: 'Email Address',
  },
  emailAddressPlaceholder: {
    id: 'app.components.OwnerDetail.emailAddressPlaceholder',
    defaultMessage: 'Email Address',
  },
  emailAddressRequiredValidation: {
    id: 'app.components.OwnerDetail.emailAddressRequiredValidation',
    defaultMessage: 'Email address is required',
  },
  emailAddressInvalid: {
    id: 'app.components.OwnerDetail.emailAddressInvalid',
    defaultMessage: 'Invalid Email Address',
  },
  cityLabel: {
    id: 'app.components.OwnerDetail.cityLabel',
    defaultMessage: 'City',
  },
  cityPlaceholder: {
    id: 'app.components.OwnerDetail.cityPlaceholder',
    defaultMessage: 'City',
  },
  cityRequiredValidation: {
    id: 'app.components.OwnerDetail.cityRequiredValidation',
    defaultMessage: 'City is required',
  },
  cityLengthValidation: {
    id: 'app.components.OwnerDetail.cityLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  cityNumberSpecialCharacterValidation: {
    id: 'app.components.OwnerDetail.cityNumberSpecialCharacterValidation',
    defaultMessage: 'Numbers or special characters not allowed',
  },
  stateLabel: {
    id: 'app.components.OwnerDetail.stateLabel',
    defaultMessage: 'State',
  },
  statePlaceholder: {
    id: 'app.components.OwnerDetail.statePlaceholder',
    defaultMessage: 'State',
  },
  stateRequiredValidation: {
    id: 'app.components.OwnerDetail.stateRequiredValidation',
    defaultMessage: 'State is required',
  },
  countryLabel: {
    id: 'app.components.OwnerDetail.countryLabel',
    defaultMessage: 'Country',
  },
  countryPlaceholder: {
    id: 'app.components.OwnerDetail.countryPlaceholder',
    defaultMessage: 'Country',
  },
  countryRequiredValidation: {
    id: 'app.components.OwnerDetail.countryRequiredValidation',
    defaultMessage: 'Country is required',
  },
  postalCodeLabel: {
    id: 'app.components.OwnerDetail.postalCodeLabel',
    defaultMessage: 'Postal Code',
  },
  postalCodePlaceholder: {
    id: 'app.components.OwnerDetail.postalCodePlaceholder',
    defaultMessage: 'Postal Code',
  },
  postalCodeRequiredValidation: {
    id: 'app.components.OwnerDetail.postalCodeRequiredValidation',
    defaultMessage: 'Postal Code is required',
  },
  postalCodeInvalid: {
    id: 'app.components.OwnerDetail.postalCodeInvalid',
    defaultMessage: 'Invalid Postal Code',
  },
  ownershipLabel: {
    id: 'app.components.OwnerDetail.ownershipLabel',
    defaultMessage: '% Ownership',
  },
  ownershipPlaceholder: {
    id: 'app.components.OwnerDetail.ownershipPlaceholder',
    defaultMessage: '% Ownership',
  },
  ownershipRequiredValidation: {
    id: 'app.components.OwnerDetail.ownershipRequiredValidation',
    defaultMessage: 'Ownership is required',
  },
  ownershipNumericValidation: {
    id: 'app.components.OwnerDetail.ownershipNumericValidation',
    defaultMessage: 'Ownership should be numeric value',
  },
  ownershipPositiveValidation: {
    id: 'app.components.OwnerDetail.ownershipPositiveValidation',
    defaultMessage: 'Ownership should be positive value',
  },
  ownershipTotalValidation: {
    id: 'app.components.OwnerDetail.ownershipTotalValidation',
    defaultMessage: 'Ownership can not be more than 100%',
  },
  ownershipDecimalPlacesValidation: {
    id: 'app.components.OwnerDetail.ownershipDecimalPlacesValidation',
    defaultMessage: 'Ownership % accepts maximum 2 decimal places',
  },
  ownershipCumulativeValidation: {
    id: 'app.components.OwnerDetail.ownershipCumulativeValidation',
    defaultMessage: 'Cumulative ownership for all owners can not be more than 100%',
  },
  ssnLabel: {
    id: 'app.components.OwnerDetail.ssnLabel',
    defaultMessage: 'Social Security Number',
  },
  ssnPlaceholder: {
    id: 'app.components.OwnerDetail.ssnPlaceholder',
    defaultMessage: 'Social Security Number',
  },
  ssnRequiredValidation: {
    id: 'app.components.OwnerDetail.ssnRequiredValidation',
    defaultMessage: 'Social Security Number is required',
  },
  ssnInvalid: {
    id: 'app.components.OwnerDetail.ssnInvalid',
    defaultMessage: 'Invalid Social Security Number',
  },
  dateOfBirthLabel: {
    id: 'app.components.OwnerDetail.dateOfBirthLabel',
    defaultMessage: 'Date Of Birth',
  },
  dateOfBirthRequiredValidation: {
    id: 'app.components.OwnerDetail.dateOfBirthRequiredValidation',
    defaultMessage: 'Date of birth is required',
  },
  dateOfBirthInvalid: {
    id: 'app.components.OwnerDetail.dateOfBirthInvalid',
    defaultMessage: 'Invalid Date',
  },
  dateOfBirthAgeValidation: {
    id: 'app.components.OwnerDetail.dateOfBirthAgeValidation',
    defaultMessage: 'Age must be greater than or equal to 18',
  },
  dateOfBirthFutureDateValidation: {
    id: 'app.components.OwnerDetail.dateOfBirthFutureDateValidation',
    defaultMessage: 'Date of birth can not be a future date',
  },
  addAnotherOwnerButton: {
    id: 'app.components.OwnerDetail.addAnotherOwnerButton',
    defaultMessage: '+ Add another owner',
  },
  removeButton: {
    id: 'app.components.OwnerDetail.removeButton',
    defaultMessage: 'Remove',
  },
  ownerHeading: {
    id: 'app.components.OwnerDetail.ownerHeading',
    defaultMessage: 'Owner',
  },
  nextButton: {
    id: 'app.components.OwnerDetail.nextButton',
    defaultMessage: 'Next',
  },
  cancelButton: {
    id: 'app.components.OwnerDetail.cancelButton',
    defaultMessage: 'Cancel',
  },
});
