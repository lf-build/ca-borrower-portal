import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import isValidAge from 'lib/validation/isValidAge';
import { decimalPlaces } from 'components/Helper/functions';
import { postalCodeRegex, emailRegex, mobileRegex, ssnRegex, dateRegex, textRegex, canadaZipCodeRegex } from 'lib/validation/regexList';
import messages from './messages';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();
  const { formatMessage } = that.props.intl;

  if (!values.owners || !values.owners.length) {
    errors.owners = { _error: 'At least one owner required' };
  } else {
    const ownerErrorsArray = [];
    values.owners.map((owner, index) => { //eslint-disable-line
      const ownerErrors = {};
       // Next button validations
      if (!owner || !owner.firstName) {
        ownerErrors.firstName = formatMessage(messages.firstNameRequiredValidation);
      } else if (!isInRange(owner.firstName, 2, 100)) {
        ownerErrors.firstName = formatMessage(messages.firstNameLengthValidation);
      } else if (!isRegexMatch(owner.firstName.trim(), textRegex)) {
        ownerErrors.firstName = formatMessage(messages.firstNameNumberSpecialCharacterValidation);
      }

      if (!owner || !owner.lastName) {
        ownerErrors.lastName = formatMessage(messages.lastNameRequiredValidation);
      } else if (!isInRange(owner.lastName, 2, 100)) {
        ownerErrors.lastName = formatMessage(messages.lastNameLengthValidation);
      } else if (!isRegexMatch(owner.lastName.trim(), textRegex)) {
        ownerErrors.lastName = formatMessage(messages.lastNameNumberSpecialCharacterValidation);
      }
      if (!owner || !owner.mobilePhone) {
        ownerErrors.mobilePhone = formatMessage(messages.mobilePhoneRequiredValidation);
      } else if (!isRegexMatch(owner.mobilePhone.replace(/ /g, ''), mobileRegex)) {
        ownerErrors.mobilePhone = formatMessage(messages.mobilePhoneInvalid);
      }
      if (!owner || !owner.homeAddress) {
        ownerErrors.homeAddress = formatMessage(messages.homeAddressRequiredValidation);
      } else if (!isInRange(owner.homeAddress, 2, 100)) {
        ownerErrors.homeAddress = formatMessage(messages.homeAddressLengthValidation);
      }
      if (!owner || !owner.emailAddress) {
        ownerErrors.emailAddress = formatMessage(messages.emailAddressRequiredValidation);
      } else if (!isRegexMatch(owner.emailAddress, emailRegex)) {
        ownerErrors.emailAddress = formatMessage(messages.emailAddressInvalid);
      }
      if (!owner || !owner.city) {
        ownerErrors.city = formatMessage(messages.cityRequiredValidation);
      } else if (!isInRange(owner.city, 2, 100)) {
        ownerErrors.city = formatMessage(messages.cityLengthValidation);
      } else if (!isRegexMatch(owner.city.trim(), textRegex)) {
        ownerErrors.city = formatMessage(messages.cityNumberSpecialCharacterValidation);
      }
      if (!owner || !owner.state) {
        ownerErrors.state = formatMessage(messages.stateRequiredValidation);
      }
      if (!owner || !owner.country) {
        ownerErrors.country = formatMessage(messages.countryRequiredValidation);
      }
      if (!owner || !owner.postalCode) {
        ownerErrors.postalCode = formatMessage(messages.postalCodeRequiredValidation);
      } else if (!isRegexMatch(owner.postalCode, owner.country === "usa" ? postalCodeRegex : canadaZipCodeRegex)) {
        ownerErrors.postalCode = formatMessage(messages.postalCodeInvalid);
      }
      if (!owner || !owner.ownership) {
        ownerErrors.ownership = formatMessage(messages.ownershipRequiredValidation);
      } else if (isNaN(owner.ownership)) {
        ownerErrors.ownership = formatMessage(messages.ownershipNumericValidation);
      } else if (owner.ownership <= 0) {
        ownerErrors.ownership = formatMessage(messages.ownershipPositiveValidation);
      } else if (owner.ownership > 100) {
        ownerErrors.ownership = formatMessage(messages.ownershipTotalValidation);
      } else if (decimalPlaces(owner.ownership) > 2) {
        ownerErrors.ownership = formatMessage(messages.ownershipDecimalPlacesValidation);
      } else if (values.owners.length > 1) {
        let totalOwnership = 0;
        values.owners.map((item) => {
          totalOwnership += Number(item.ownership);
          return true;
        });
        if (totalOwnership > 100) {
          ownerErrors.ownership = formatMessage(messages.ownershipCumulativeValidation);
        }
      }
      if (!owner || !owner.ssn) {
        ownerErrors.ssn = formatMessage(messages.ssnRequiredValidation);
      } else if (!isRegexMatch(owner.ssn, ssnRegex)) {
        ownerErrors.ssn = formatMessage(messages.ssnInvalid);
      }
      if (!owner || !owner.dateOfBirth) {
        ownerErrors.dateOfBirth = formatMessage(messages.dateOfBirthRequiredValidation);
      } else if (!isRegexMatch(owner.dateOfBirth, dateRegex)) {
        ownerErrors.dateOfBirth = formatMessage(messages.dateOfBirthInvalid);
      } else if (!isValidAge(owner.dateOfBirth, 18)) {
        ownerErrors.dateOfBirth = formatMessage(messages.dateOfBirthAgeValidation);
      } else {
        const parts = owner.dateOfBirth.split('/');
        const mydate = new Date(parts[2], parts[0] - 1, parts[1]);
        if (mydate >= new Date()) {
          ownerErrors.dateOfBirth = formatMessage(messages.dateOfBirthFutureDateValidation);
        }
      }
      ownerErrorsArray[index] = ownerErrors;
    });
    if (ownerErrorsArray.length) {
      errors.owners = ownerErrorsArray;
    }
  }
  return errors;
};

export default validateForm;
