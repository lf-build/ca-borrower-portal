/**
*
* DrawdownList
*
*/

import React from 'react';
import Spinner from 'components/Spinner';
import { dateWithMMDDYYYY, moneyFormtter } from 'components/Helper/formatting';
import { injectIntl, intlShape } from 'react-intl';
import messages from './messages';

class DrawdownList extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const drawdowns = this.props.drawdownsList;
    const { formatMessage } = this.props.intl;
    return (
      <div className="col-sm-12 login-box">
        <div className="panel-group todo-panel-group" id="accordion">
          <div className="panel panel-default">
            <div className="panel-heading">
              <h4 className="panel-title">
                <a className="accordion-toggle">
                  {formatMessage(messages.header)} <span ></span>
                </a>
              </h4>
            </div>
            <div id="collapseOne" className="panel-collapse collapse in" >
              <div className="panel-body">
                <div className="transactions-detail">
                  { drawdowns !== null ?
                    <table id="example" className="table table-striped">
                      <thead>
                        <tr>
                          <th>{formatMessage(messages.drawdownTableHeader)}</th>
                          <th>{formatMessage(messages.amountTableHeader)}</th>
                          <th>{formatMessage(messages.dateTableHeader)}</th>
                          <th>{formatMessage(messages.statusTableHeader)}</th>
                        </tr>
                      </thead>
                      <tbody>
                        {drawdowns.length > 0 ?
                              drawdowns.map((elem, index) => (
                                <tr key={index}>
                                  <td className="td-center">{elem.drawDownId}</td>
                                  <td className={elem.requestedAmount && elem.requestedAmount * -1 < 0 ? 'td-amount red-font-color' : 'td-amount'}>
                                    {moneyFormtter(elem.requestedAmount)}
                                  </td>
                                  <td className="td-center">{dateWithMMDDYYYY(elem.lastProgressDate.time)}</td>
                                  <td className="td-center">{elem.statusName}</td>
                                </tr>
                              ))
                              : <tr><td colSpan="4"><span>{formatMessage(messages.noDrawAvailableMessage)}</span></td> </tr>
                            }
                      </tbody>
                    </table> : <Spinner />
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DrawdownList.propTypes = {
  drawdownsList: React.PropTypes.any,
  intl: intlShape.isRequired,
};

export default injectIntl(DrawdownList);

