/*
 * DrawdownList Messages
 *
 * This contains all the text for the DrawdownList component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DrawdownList.header',
    defaultMessage: 'Draw Details',
  },
  drawdownTableHeader: {
    id: 'app.components.DrawdownList.drawdownTableHeader',
    defaultMessage: 'Drawdown',
  },
  amountTableHeader: {
    id: 'app.components.DrawdownList.amountTableHeader',
    defaultMessage: 'Amount',
  },
  dateTableHeader: {
    id: 'app.components.DrawdownList.dateTableHeader',
    defaultMessage: 'Date',
  },
  statusTableHeader: {
    id: 'app.components.DrawdownList.statusTableHeader',
    defaultMessage: 'Status',
  },
  noDrawAvailableMessage: {
    id: 'app.components.DrawdownList.noDrawAvailableMessage',
    defaultMessage: 'No drawdowns available',
  },
});

