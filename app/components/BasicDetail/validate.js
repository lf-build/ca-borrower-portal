import isInRange from 'lib/validation/isInRange';
import isRegexMatch from 'lib/validation/isRegexMatch';
import { emailRegex, mobileRegex, passwordRegex, textRegex } from 'lib/validation/regexList';
import messages from './messages';

const validateForm = (that, _values) => {
  const errors = {};
  const values = _values.toJS();
  const { formatMessage } = that.props.intl;

  // Next button validations
  if (!values.requestedAmount) {
    errors.requestedAmount = formatMessage(messages.requestedAmountRequiredValidation);
  }

  if (!values.firstName) {
    errors.firstName = formatMessage(messages.firstNameRequiredValidation);
  } else if (!isInRange(values.firstName, 2, 100)) {
    errors.firstName = formatMessage(messages.firstNameLengthValidation);
  } else if (!isRegexMatch(values.firstName.trim(), textRegex)) {
    errors.firstName = formatMessage(messages.firstNameNumberSpecialCharacterValidation);
  }

  if (!values.lastName) {
    errors.lastName = formatMessage(messages.lastNameRequiredValidation);
  } else if (!isInRange(values.lastName, 2, 100)) {
    errors.lastName = formatMessage(messages.lastNameLengthValidation);
  } else if (!isRegexMatch(values.lastName.trim(), textRegex)) {
    errors.lastName = formatMessage(messages.lastNameNumberSpecialCharacterValidation);
  }

  if (!values.loanTimeFrame) {
    errors.loanTimeFrame = formatMessage(messages.loanTimeFrameRequiredValidation);
  }

  if (!values.purposeOfLoan) {
    errors.purposeOfLoan = formatMessage(messages.purposeOfLoanRequiredValidation);
  }

  if (!values.primaryPhone) {
    errors.primaryPhone = formatMessage(messages.primaryPhoneRequiredValidation);
  } else if (!isRegexMatch(values.primaryPhone.replace(/ /g, ''), mobileRegex)) {
    errors.primaryPhone = formatMessage(messages.primaryPhoneInvalid);
  }

  if (!values.emailAddress) {
    errors.emailAddress = formatMessage(messages.emailAddressRequiredValidation);
  } else if (!isRegexMatch(values.emailAddress, emailRegex)) {
    errors.emailAddress = formatMessage(messages.emailAddressInvalid);
  }

  if (!values.password) {
    errors.password = formatMessage(messages.passwordRequiredValidation);
  } else if (!isRegexMatch(values.password, passwordRegex)) {
    errors.password = formatMessage(messages.passwordRegexValidation);
  }

  if (!values.confirmPassword) {
    errors.confirmPassword = formatMessage(messages.confirmPasswordRequiredValidation);
  } /* else if (!isRegexMatch(values.confirmPassword, passwordRegex)) {
    errors.confirmPassword = 'Confirm password must be eight characters including one uppercase letter, one special character and alphanumeric characters';
  }*/

  if (values.password && values.confirmPassword && values.password !== values.confirmPassword) {
    errors.confirmPassword = formatMessage(messages.confirmPasswordMismatchPassword);
  }

  return errors;
};

export default validateForm;
