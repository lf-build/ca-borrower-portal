/*
 * BasicDetail Messages
 *
 * This contains all the text for the BasicDetail component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  firstNameLabel: {
    id: 'app.components.BasicDetail.firstNameLabel',
    defaultMessage: 'First Name',
  },
  firstNamePlaceholder: {
    id: 'app.components.BasicDetail.firstNamePlaceholder',
    defaultMessage: 'First Name',
  },
  firstNameRequiredValidation: {
    id: 'app.components.BasicDetail.firstNameRequiredValidation',
    defaultMessage: 'First Name is required',
  },
  firstNameLengthValidation: {
    id: 'app.components.BasicDetail.firstNameLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  firstNameNumberSpecialCharacterValidation: {
    id: 'app.components.BasicDetail.firstNameNumberSpecialCharacterValidation',
    defaultMessage: 'Numbers or special characters not allowed',
  },
  lastNameLabel: {
    id: 'app.components.BasicDetail.lastNameLabel',
    defaultMessage: 'Last Name',
  },
  lastNamePlaceholder: {
    id: 'app.components.BasicDetail.lastNamePlaceholder',
    defaultMessage: 'Last Name',
  },
  lastNameRequiredValidation: {
    id: 'app.components.BasicDetail.lastNameRequiredValidation',
    defaultMessage: 'Last Name is required',
  },
  lastNameLengthValidation: {
    id: 'app.components.BasicDetail.lastNameLengthValidation',
    defaultMessage: 'Field length must be between 2 and 100',
  },
  lastNameNumberSpecialCharacterValidation: {
    id: 'app.components.BasicDetail.lastNameNumberSpecialCharacterValidation',
    defaultMessage: 'Numbers or special characters not allowed',
  },
  primaryPhoneLabel: {
    id: 'app.components.BasicDetail.primaryPhoneLabel',
    defaultMessage: 'Primary Phone',
  },
  primaryPhonePlaceholder: {
    id: 'app.components.BasicDetail.primaryPlaceholder',
    defaultMessage: 'Primary Phone #',
  },
  primaryPhoneRequiredValidation: {
    id: 'app.components.BasicDetail.primaryPhoneRequiredValidation',
    defaultMessage: 'Primary Phone is required',
  },
  primaryPhoneInvalid: {
    id: 'app.components.BasicDetail.primaryPhoneInvalid',
    defaultMessage: 'Invalid Primary Phone',
  },
  passwordLabel: {
    id: 'app.components.BasicDetail.passwordLabel',
    defaultMessage: 'Password',
  },
  passwordPlaceholder: {
    id: 'app.components.BasicDetail.passwordPlaceholder',
    defaultMessage: 'Password',
  },
  passwordRequiredValidation: {
    id: 'app.components.BasicDetail.passwordRequiredValidation',
    defaultMessage: 'Password is required',
  },
  passwordRegexValidation: {
    id: 'app.components.BasicDetail.passwordRegexValidation',
    defaultMessage: 'Password must be eight characters including one uppercase letter, one special character and alphanumeric characters',
  },
  confirmPasswordLabel: {
    id: 'app.components.BasicDetail.confirmPasswordLabel',
    defaultMessage: 'Confirm Password',
  },
  confirmPasswordPlaceholder: {
    id: 'app.components.BasicDetail.confirmPasswordPlaceholder',
    defaultMessage: 'Confirm Password',
  },
  confirmPasswordRequiredValidation: {
    id: 'app.components.BasicDetail.confirmPasswordRequiredValidation',
    defaultMessage: 'Confirm Password is required',
  },
  confirmPasswordMismatchPassword: {
    id: 'app.components.BasicDetail.confirmPasswordMismatchPassword',
    defaultMessage: 'Confirm password is not the same as password',
  },
  emailAddressLabel: {
    id: 'app.components.BasicDetail.emailAddressLabel',
    defaultMessage: 'Username',
  },
  emailAddressPlaceholder: {
    id: 'app.components.BasicDetail.emailAddressPlaceholder',
    defaultMessage: ' Email Address',
  },
  emailAddressRequiredValidation: {
    id: 'app.components.BasicDetail.emailAddressRequiredValidation',
    defaultMessage: 'Username is required',
  },
  emailAddressInvalid: {
    id: 'app.components.BasicDetail.emailAddressInvalid',
    defaultMessage: 'Invalid Email Address',
  },
  requestedAmountLabel: {
    id: 'app.components.BasicDetail.requestedAmountLabel',
    defaultMessage: 'How much do you need?',
  },
  requestedAmountRequiredValidation: {
    id: 'app.components.BasicDetail.requestedAmountRequiredValidation',
    defaultMessage: 'Requested Amount is required',
  },
  loanTimeFrameLabel: {
    id: 'app.components.BasicDetail.loanTimeFrameLabel',
    defaultMessage: 'How soon do you need it?',
  },
  loanTimeFrameRequiredValidation: {
    id: 'app.components.BasicDetail.loanTimeFrameRequiredValidation',
    defaultMessage: 'Loan time frame is required',
  },
  purposeOfLoanLabel: {
    id: 'app.components.BasicDetail.purposeOfLoanLabel',
    defaultMessage: 'Purpose of funds?',
  },
  purposeOfLoanRequiredValidation: {
    id: 'app.components.BasicDetail.purposeOfLoanRequiredValidation',
    defaultMessage: 'Purpose of loan is required',
  },
  nextButton: {
    id: 'app.components.BasicDetail.nextButton',
    defaultMessage: 'Next',
  },
  cancelButton: {
    id: 'app.components.BasicDetail.cancelButton',
    defaultMessage: 'Cancel',
  },
});
