/*
 * ApplicantSortDetails Messages
 *
 * This contains all the text for the ApplicantSortDetails component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  loanApplied: {
    id: 'app.components.ApplicantSortDetails.loanApplied',
    defaultMessage: 'Loan Applied',
  },
  purposeOfFund: {
    id: 'app.components.ApplicantSortDetails.purposeOfFund',
    defaultMessage: 'Purpose of funds',
  },
  applicationNumberHeading: {
    id: 'app.components.ApplicantSortDetails.applicationNumberHeading',
    defaultMessage: 'App',
  },
  loan: {
    id: 'app.components.ApplicantSortDetails.loan',
    defaultMessage: 'Loan',
  },
  reapplyButton: {
    id: 'app.components.ApplicantSortDetails.reapplyButton',
    defaultMessage: 'Reapply',
  },
  reapplyPopupHeader: {
    id: 'app.components.ApplicantSortDetails.reapplyPopupHeader',
    defaultMessage: 'Re-Apply Confirmation Message',
  },
  reapplyPopupBody: {
    id: 'app.components.ApplicantSortDetails.reapplyPopupBody',
    defaultMessage: 'Are you sure you want to re-apply the loan application ?',
  },
  reapplyPopupSuccessButton: {
    id: 'app.components.ApplicantSortDetails.reapplyPopupSuccessButton',
    defaultMessage: 'Yes',
  },
  reapplyPopupCloseButton: {
    id: 'app.components.ApplicantSortDetails.reapplyPopupCloseButton',
    defaultMessage: 'No',
  },
});
