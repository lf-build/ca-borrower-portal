/**
*
* ApplicantSortDetails
*
*/

import React from 'react';
import { connect } from 'react-redux';
import { withRouter, browserHistory } from 'react-router';
import { createStructuredSelector } from 'reselect';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { injectIntl, intlShape, FormattedNumber } from 'react-intl';
import Spinner from 'components/Spinner';
import PopupOpen from 'components/PopupOpen';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';
import authCheck from 'components/Helper/authCheck';
import makeAuthSelector from 'sagas/auth/selectors';
import updateUser from './updateUser';
import messages from './messages';

class ApplicantSortDetails extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { displayPopupForReApply: false, applicationNumber: (this.props.auth && this.props.auth.user && this.props.auth.user.applicationNumber) ? this.props.auth.user.applicationNumber : undefined };
  }
  componentDidMount() {
    if (this.state.applicationNumber === undefined) {
      // if applicationNumber is undefinded and dashboard array exists, move to daashbaord page
      if (this.props.auth && this.props.auth.dashboard && this.props.auth.dashboard.length > 0) {
        browserHistory.replace('/dashboard');
      } else {
        redirectWithReturnURL(this.props.location.pathname);
      }
    } else {
      // this.props.dispatch(loadUserProfile({ userId: this.props.auth.user.userId }));
      // Using execute to fetch auth user instead of saga method and it will be passed to saga method to update auth.user
      execute(undefined, undefined, ...'api/ca/update-user'.split('/'), { applicationNumber: this.props.auth.user.applicationNumber })
           .then((response) => {
             updateUser(this.props.dispatch)(response)(this.props.auth.user);
           }).catch((e) => {
             authCheck(this.props.dispatch)(e)(this.props.location.pathname);
           }
          );
    }
  }
  onSuccess = () => {
    browserHistory.replace('/home');
  }
  hidePopup = () => {
    this.displayPopup(false);
  }
  displayPopup = (state) => {
    this.setState({ displayPopupForReApply: state });
  }
  render() {
    const { appMode, auth: { user } } = this.props;
    if (!appMode) {
      return (<span />);
    }
    if (!user) {
      return <Spinner />;
    }
    const { formatMessage } = this.props.intl;
    return (
      <div className="main-header clearfix">
        <div className="page-title pull-left">
          <h3 className="no-margin">{user.userName}</h3>
          {user.loanNumber && user.loanNumber !== null && user.lmsAccess && <span><i className="fa fa-file-text-o"></i>{formatMessage(messages.loan)}#:{user.loanNumber}</span>}
          <span><i className="fa fa-file-text-o"></i>{formatMessage(messages.applicationNumberHeading)}#:{user.applicationNumber}</span>
          <span>{ user.isEligibleForReapply && <button onClick={() => this.displayPopup(true)} data-backdrop="static" data-toggle="modal" data-target="#exampleModal" className="btn btn-default">{formatMessage(messages.reapplyButton)}</button>}</span>
        </div>
        <ul className="page-stats">
          <li>
            <div className="value">
              <span>{formatMessage(messages.loanApplied)}</span>
              <h4 id="currentVisitor"><FormattedNumber {...{ value: user.loanAmount, style: 'currency', currency: 'USD', maximumFractionDigits: 2, minimumFractionDigits: 0 }} /></h4>
            </div>
          </li>
          <li>
            <div className="value">
              <span>{formatMessage(messages.purposeOfFund)}</span>
              <h4><strong id="currentBalance">{user.loanPurpose}</strong></h4>
            </div>
          </li>
        </ul>
        <PopupOpen onSuccess={this.onSuccess} displayPopup={this.state.displayPopupForReApply} hidePopup={this.hidePopup} headerMessage={formatMessage(messages.reapplyPopupHeader)} content={formatMessage(messages.reapplyPopupBody)} successButtonText={formatMessage(messages.reapplyPopupSuccessButton)} cancelButtonText={formatMessage(messages.reapplyPopupCloseButton)} />
      </div>
    );
  }
}

ApplicantSortDetails.propTypes = {
  appMode: React.PropTypes.bool,
  auth: React.PropTypes.object,
  intl: intlShape.isRequired,
  location: React.PropTypes.object.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  auth: makeAuthSelector(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(withRouter(ApplicantSortDetails)));
