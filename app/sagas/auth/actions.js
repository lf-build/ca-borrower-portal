/*
 *
 * SignInPage actions
 *
 */

import {
  DEFAULT_ACTION,
  USER_SIGNED_IN,
  USER_SIGNED_OUT,
  USER_INFO_AVAILABLE,
  LOAD_PROFILE,
  ADD_UPDATE_USER,
  CLEAR_STATE,
  LOAD_DASHBOARD_APPLICATION,
  DASHBOARD_INFO_AVAILABLE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function userSignedIn(token, rememberMe, userId) {
  return {
    type: USER_SIGNED_IN,
    meta: {
      token,
      rememberMe,
      userId,
    },
  };
}

export function loadDashboardApplication(meta) {
  return {
    type: LOAD_DASHBOARD_APPLICATION,
    meta,
  };
}

export function loadUserProfile(meta) {
  return {
    type: LOAD_PROFILE,
    meta,
  };
}

export function addUpdateUser(user) {
  return {
    type: ADD_UPDATE_USER,
    meta: {
      user,
    },
  };
}

export function userInfoAvailable(user) {
  return {
    type: USER_INFO_AVAILABLE,
    meta: {
      user,
    },
  };
}

export function dashboardInfoAvailable(dashboard) {
  return {
    type: DASHBOARD_INFO_AVAILABLE,
    meta: {
      dashboard,
    },
  };
}

export function userSignedOut(redirectPage) {
  return {
    type: USER_SIGNED_OUT,
    meta: {
      redirectPage,
    },
  };
}

export function clearState(pathName) {
  return {
    type: CLEAR_STATE,
    meta: {
      pathName,
    },
  };
}
