/*
 *
 * SignInPage constants
 *
 */

export const DEFAULT_ACTION = 'app/SignInPage/DEFAULT_ACTION';
export const USER_SIGNED_IN = 'app/SignInPage/USER_SIGNED_IN';
export const LOAD_PROFILE = 'app/SignInPage/LOAD_PROFILE';
export const USER_INFO_AVAILABLE = 'app/SignInPage/USER_INFO_AVAILABLE';
export const USER_SIGNED_OUT = 'app/SignInPage/USER_SIGNED_OUT';
export const CLEAR_STATE = 'app/SignInPage/CLEAR_STATE';
export const LOAD_DASHBOARD_APPLICATION = 'app/SignInPage/LOAD_DASHBOARD_APPLICATION';
export const DASHBOARD_INFO_AVAILABLE = 'app/SignInPage/DASHBOARD_INFO_AVAILABLE';
export const ADD_UPDATE_USER = 'app/SignInPage/ADD_UPDATE_USER';

