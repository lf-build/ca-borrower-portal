import { takeEvery, put, select } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import redirectWithReturnURL from 'components/Helper/redirectWithReturnURL';
import { userInfoAvailable, dashboardInfoAvailable } from './actions';
import { LOAD_PROFILE, USER_SIGNED_OUT, LOAD_DASHBOARD_APPLICATION, CLEAR_STATE } from './constants';
import makeAuthSelector from '../../sagas/auth/selectors';

function* loginSaga(action) {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'api',
    section: 'ca',
    command: 'get-applicant-basic-detail',
  }, {
    tag: 'who-am-i',
    payload: {
      userId: action.meta.userId,
    },
  }))) {
    return;
  }
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('who-am-i');
  if (valid) {
    yield put(userInfoAvailable(valid.meta.body));
  } else {
    // throw new Error('Invalid token 2');
  }
}

function* logoutSaga(action) {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'authorization',
    section: 'identity',
    command: 'logout',
  }, {
    tag: 'sign-out',
  }))) {
    return;
  }
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('sign-out');
  if (valid) {
    yield put({
      type: 'RESET',
      state: undefined,
    });
    localStorage.clear();
    if (action.meta && action.meta.redirectPage) {
      browserHistory.replace(`/${action.meta.redirectPage}`);
    } else {
      browserHistory.replace('/home');
    }
    window.location.reload();
  } else {
    // throw new Error('Invalid token');
  }
}

function* checkSession() {
  const pathName = browserHistory.getCurrentLocation().pathname;
  if (pathName === '/login' || pathName === '/forgot-password' || pathName === '/home' || pathName === '/reset-account') {
    return;
  }
  const auth = (yield select(makeAuthSelector()));
  if (auth && auth.token) {
    return;
  }
  setTimeout(() => {
    // browserHistory.replace('/login');
  }, 1);
}

function* signedOut(action) {
  const pathName = browserHistory.getCurrentLocation().pathname;
  if (action.meta.pathName) {
    redirectWithReturnURL(action.meta.pathName);
  } else {
    if (pathName === '/login' || pathName === '/home' || pathName === 'terms-conditions') {
      return;
    }
    browserHistory.replace('/login');
  }
}

function* loadDashboard(action) {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'api',
    section: 'application',
    command: 'dashboard',
  }, {
    tag: 'load-dashbaord',
    payload: {
      userId: action.meta.userId,
    },
  }))) {
    return;
  }
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('load-dashbaord');
  if (valid) {
    yield put(dashboardInfoAvailable(valid.meta.body));
  } else {
    // throw new Error('Invalid token 2');
  }
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(LOAD_PROFILE, loginSaga);
  yield takeEvery(USER_SIGNED_OUT, logoutSaga);
  yield takeEvery(LOAD_DASHBOARD_APPLICATION, loadDashboard);
  yield takeEvery('@@router/LOCATION_CHANGE', checkSession);
  yield takeEvery(CLEAR_STATE, signedOut);
}

// All sagas to be loaded
export default [
  defaultSaga,
];

