/*
 *
 * auth reducer
 *
 */

import { fromJS } from 'immutable';
import {
  USER_SIGNED_OUT,
  USER_SIGNED_IN,
  USER_INFO_AVAILABLE,
  CLEAR_STATE,
  DASHBOARD_INFO_AVAILABLE,
  ADD_UPDATE_USER,
} from './constants';

const initialState = fromJS({ forceRerender: 0 });

// forceRerender is added to achive shallowEqual
// As reselect consider first level of state update. So to forcefully rerender the component
// we added forceRerender under auth and increment its count at each user uddate call
// https://github.com/reactjs/redux/issues/585

function authReducer(state = initialState, action) {
  switch (action.type) {
    case USER_SIGNED_IN:
      return state
      .set('token', action.meta.token)
      .set('rememberMe', action.meta.rememberMe)
      .set('userId', action.meta.userId);
    case USER_INFO_AVAILABLE:
    case ADD_UPDATE_USER:
      return state
      .set('user', action.meta.user)
      .set('forceRerender', state.get('forceRerender') + 1);
    case DASHBOARD_INFO_AVAILABLE:
      return state
      .set('dashboard', action.meta.dashboard);
    case USER_SIGNED_OUT:
    case CLEAR_STATE:
      return state
       .set('user', undefined)
       .set('rememberMe', undefined)
       .set('token', undefined)
       .set('userId', undefined)
       .set('dashboard', undefined);
    default:
      return state;
  }
}

export default authReducer;
