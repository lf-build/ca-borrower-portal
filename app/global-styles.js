import { injectGlobal } from 'styled-components';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';

export default () => fetchStyles().then(applyStyles);

const applyStyles = ({ successPanelColor, infoPanelColor, bodyColor, bodyBackground, linkColor, sideNavBackground, sideNavLinkColor, headerBackground, logoSepColor, rightMenuList, rightMenuLinkHoverFocus, rightMenuBottomBorder, rightMenuBackGround, rightMenuColor, dropDownMenuBorderColor, mainHeaderBorderBottom, mainHeaderBackground, mainHeaderColor,
mainHeaderSpanColor, mainHeaderSpanTextShadow, mainHeaderListBorderRight, mainHeaderValueSpanColor, breadcrumb, mainHeaderH4Color, panelHeadingBackground, panelHeadingColor, panelHeadingBorderColor, activityIconBackground, activityIconBorder, activityIconBorderColor, activityIconColor, bgSuccessBackgroundColor, bgSuccessColor, userBlockBorderBottom, userBlockBoxShadow,
userBlockDetail, labelDangerBackgroundColor, labelDanger, bgInfoBackGroundColor, bgInfo, bgSuccessListBackGroundColor, bgSuccessListColor, bgSuccessListBorderColor, bgSuccessListBadgeBackGroundColor, bgSuccessListBadgeColor, docUploadContainerBorder, docUploadAreaBorder, docUploadAreaBackground, btnDefaultBackground, btnDefaultBorder, btnDefault, selectFields, accountBtn,
bgInfoPanelHeadingBackGround, bgInfoPanelHeadingBorderColor, errorMessage, bLogoBackGround, bLogoBorder, bLogoBoxShadow, logoText, currentVisitorSpan, breadcrumbBorderBottom, breadcrumbList }) => {
  const style = `
.btn.btn-default,.list-group.bg-success > li,.main-header .more-link,.activity-icon.bg-success{background:${successPanelColor}!important;}
.list-group.bg-success > li .badge{color:${successPanelColor}!important;}
.panel.bg-info{background:${infoPanelColor}!important;}
body {
    color: ${bodyColor};
    background: ${bodyBackground};
}
a {
    color: ${linkColor};
}
aside{
    background: ${sideNavBackground};
}
aside .main-menu > ul > li > a {
    color: ${sideNavLinkColor};
}
header.navbar{
    background-color: ${headerBackground};
}
header .navbar-header {
    background: ${headerBackground};
    border: 3px solid ${headerBackground};
}
header .navbar-brand.lendfoundry-logo {
    border-left: 1px solid ${logoSepColor};
}

header.navbar #right-menu > ul > li >a {
    color: ${rightMenuList};
}
header.navbar #right-menu .dropdown-menu li {
    border-bottom-color: ${rightMenuBottomBorder};
}
header.navbar #right-menu > ul > li > a:hover, #top-nav .nav-notification > li > a:focus {
    color: ${rightMenuLinkHoverFocus};
}
header.navbar #right-menu > ul > li.open {
    background: ${rightMenuBackGround};
}
header.navbar #right-menu > ul > li.open a{background:none;}
header.navbar #right-menu > ul > li > a {
    color: ${rightMenuList};
}
header.navbar #right-menu > ul > li > a:focus,header.navbar #right-menu > ul > li >a:hover {
    color: ${rightMenuColor};
}

header.navbar #right-menu .dropdown-menu {
    border-color: ${dropDownMenuBorderColor};
}
.main-header {
border-bottom: 1px solid ${mainHeaderBorderBottom};
}
.main-header .more-link {
    background: ${mainHeaderBackground} none repeat scroll 0 0;
    color: ${mainHeaderColor};
}
.main-header .page-title span {
    color: ${mainHeaderSpanColor};
    text-shadow: 0 1px 0 ${mainHeaderSpanTextShadow};
}
.main-header .page-stats li {
    border-right: 1px solid ${mainHeaderListBorderRight};
}

.main-header .page-stats li .value span {
    color: ${mainHeaderValueSpanColor};
    text-shadow: 0 1px 0 ${mainHeaderColor};
}
.main-header .page-stats li .value h4 {
    color: ${mainHeaderH4Color};
}
.breadcrumb {
    background: 0 0;
    border-bottom: 1px solid ${breadcrumbBorderBottom};
    box-shadow: 0 1px 0 ${breadcrumb};
    -moz-box-shadow: 0 1px 0 ${breadcrumb};
    -webkit-box-shadow: 0 1px 0 ${breadcrumb};
    text-shadow: 0 1px ${breadcrumb};
}
.breadcrumb li {
    color: ${breadcrumbList};
}
.breadcrumb a {
    color: ${breadcrumbList};
}

.panel.panel-default .panel-heading {
    background: ${panelHeadingBackground};
    color: ${panelHeadingColor};
    border-color: ${panelHeadingBorderColor};
}
.activity-icon {
    background: ${activityIconBackground};
    border: 2px solid ${activityIconBorder};
    border-color: ${activityIconBorderColor};
    color: ${activityIconColor};
}

.bg-success {
    background-color: ${bgSuccessBackgroundColor};
    color: ${bgSuccessColor};
}

.main-header .page-stats li .value span {
    font-size: 11px;
    font-weight: 600;
    text-transform: uppercase;
}

.main-header .page-stats li .value h4 {
    font-size: 20px;
    line-height: 20px;
    font-weight: 600;
    color: ${mainHeaderH4Color};
}

aside .sidebar-inner .user-block .detail p{font-size:11px;}
.todo-panel-group .panel-title  .activity-icon{
color:${activityIconColor}; 
}

.panel.bg-info .panel-heading {
    font-size: 14px;
    font-weight: 700;
}
.doc-upload-container > h3 {
    font-size: 16px;
    font-weight: 700;
}

.doc-upload-text > span {
    font-size: 15px;
    line-height: 50px;
}


.doc-upload-text > span i {
font-size: 38px;
}

aside .sidebar-inner .user-block {
    padding: 10px;
    border-bottom: 1px solid ${userBlockBorderBottom};
    box-shadow: 0 1px 0 ${userBlockBoxShadow};
    -moz-box-shadow: 0 1px 0 ${userBlockBoxShadow};
    -webkit-box-shadow: 0 1px 0 ${userBlockBoxShadow};
}
aside .sidebar-inner .user-block .detail {
    color: ${userBlockDetail};
}
.badge-danger, .label-danger {
    background-color: ${labelDangerBackgroundColor};
    color: ${labelDanger};
}
.bg-info {
    background-color: ${bgInfoBackGroundColor};
    color: ${bgInfo};
}
.panel.bg-info {
    color: ${bgInfo};
}
.panel.bg-info .panel-heading {
  background: ${bgInfoPanelHeadingBackGround} none repeat scroll 0 0;
    border-color:${bgInfoPanelHeadingBorderColor};
    color: ${bgInfo};
}
.list-group.bg-success{background:none;}
.list-group.bg-success > li{background-color: ${bgSuccessListBackGroundColor};
   color: ${bgSuccessListColor};
   border-color:${bgSuccessListBorderColor};}
.list-group.bg-success > li .badge {
    background-color: ${bgSuccessListBadgeBackGroundColor};
    color:${bgSuccessListBadgeColor}}
.doc-upload-container {
    border: 1px solid ${docUploadContainerBorder};
}
.doc-upload-area {
    border: 1px dashed ${docUploadAreaBorder};  
    border-radius: 3px;
    background: ${docUploadAreaBackground};
}
.btn.btn-default {
background:${btnDefaultBackground};
border-color:${btnDefaultBorder};
color:${btnDefault}
}

.form-control{font-size:12px;}
.select_fields{font-size:12px;color: ${selectFields};}

/*------- Font Style ---------*/
body {
    font-size: 12px;
    font-family: 'Open Sans',sans-serif;
}
b, strong {
    font-weight: 700;
}
aside .main-menu > ul > li > a {
    font-size: 13px;
}
.main-header .page-stats li .value span {
    font-size: 11px;
    font-weight: 600;
    text-transform: uppercase;
}
.main-header .page-stats li .value h4 {
    font-size: 20px;
    line-height: 20px;
    font-weight: 600;
}
.breadcrumb {
    font-size: 11px;
    font-weight: 700;
}
.breadcrumb li {
    line-height: 27px;
}
.breadcrumb a {
    text-decoration: none;
}
.panel.panel-default .panel-title a {
    font-size: 12px;
}
.activity-icon.small [class*=fa-] {
    line-height: 30px;
}
.activity-icon.small {
    font-size: 20px;
}

.panel.panel-default .panel-title a {
    font-size: 14px;
    line-height: 30px;
}
.panel.todo-panel-group .panel-title a {
    font-size: 14px;
    line-height: 30px;
}
aside .sidebar-inner .user-block .detail p{ font-size:11px;}
.panel.bg-info .panel-heading {
    font-size: 14px;
    font-weight: 700;
}
.doc-upload-container > h3 {
    font-size: 16px;
    font-weight: 700;
}
.doc-upload-text > span {
    font-size: 15px;
    line-height: 50px;
}
.doc-upload-text > span i {
    font-size: 38px;
}

.account_btn {
    font-size: 15px;
    font-weight: 500;
    color: ${accountBtn};
}

.privacy-text{font-size:13px;}
.done-heading {
    font-size: 18px;
    text-align: center;
}
.done-text {
    font-size: 13px;
    text-align: center;
}
.error_message{color:${errorMessage}; display:inline-block;}
.b-logo { 
  background:${bLogoBackGround};
  border: 2px solid ${bLogoBorder};
  box-shadow:inset 0px 0px 0px 2px ${bLogoBoxShadow};}
.logo-text {color: ${logoText};}
h4#currentVisitor span{color: ${currentVisitorSpan};}`;

/* eslint no-unused-expressions: 0 */
  injectGlobal`${style}`;
};

const fetchStyles = () => execute(undefined, undefined, 'configuration', 'color-scheme', 'fetch', { key: 'borrower' }).then(({ body }) => body);

/* Promise.resolve({
       "successPanelColor": "#65cea7",
        "infoPanelColor": "#6bafbd",
        "bodyColor": "#777",
        "bodyBackground": "#f9f9f9",
        "linkColor": "#4db08b",
        "sideNavBackground": "#495b6c",
        "sideNavLinkColor": "#BECFE0",
        "headerBackground": "#fff",
        "logoSepColor": "#dadada",
        "rightMenuList": "#BECFE0",
        "rightMenuLinkHoverFocus": "#fff",
        "rightMenuBottomBorder": "#f1f5fc",
        "rightMenuBackGround":"#415160",
        "rightMenuColor": "#777",
        "dropDownMenuBorderColor": "#f1f5fc",
        "mainHeaderBorderBottom": "#eee",
        "mainHeaderBackground": "#65cea7",
        "mainHeaderColor": "#fff",
        "mainHeaderSpanColor": "#999",
        "mainHeaderSpanTextShadow": "#fff",
        "mainHeaderListBorderRight": "#ccc",
        "mainHeaderValueSpanColor": "#bbb",
        "breadcrumb":"#fff",
        "mainHeaderH4Color":"#777",
        "panelHeadingBackground": "#fff",
        "panelHeadingColor":"#777",
        "panelHeadingBorderColor":"#f1f5fc",
        "activityIconBackground":"#999",
        "activityIconBorder": "rgba(0,0,0,.2)",
        "activityIconBorderColor": "rgba(255,255,255,.5)",
        "activityIconColor": "#fff",
        "bgSuccessColor":"#fff",
        "bgSuccessBackgroundColor": "#65cea7",
        "userBlockBorderBottom": "#415160",
        "userBlockBoxShadow": "#34364a",
        "userBlockDetail": "#e6f1f7",
        "labelDanger": "#fff",
        "labelDangerBackgroundColor":"#fc8675",
        "bgInfo":"#fff",
        "bgInfoBackGroundColor":"#6bafbd",
        "bgSuccessListBackGroundColor":"#65cea7",
        "bgSuccessListColor":"#fff",
        "bgSuccessListBorderColor":"rgba(255, 255, 255, 0.5)",
        "bgSuccessListBadgeBackGroundColor":"#fff",
        "bgSuccessListBadgeColor":"#65cea7",
        "docUploadContainerBorder":"#ddd",
        "docUploadAreaBorder":"rgba(0, 0, 0, 0.1)",
        "docUploadAreaBackground":"rgba(0,0,0,0.03)",
        "btnDefault":"#fff",
        "btnDefaultBackground":"#65cea7",
        "btnDefaultBorder":"rgba(0, 0, 0, 0.2)",
        "selectFields":"#555",
        "accountBtn":"#FFF",
        "bgInfoPanelHeadingBackGround":"rgba(20, 20, 20, 0.07)",
        "bgInfoPanelHeadingBorderColor":"rgba(0,0,0,.1)",
        "errorMessage": "#D10000",
        "bLogoBackGround":"#6bafbd",
        "bLogoBorder":"#6bafbd",
        "bLogoBoxShadow":"#fff",
        "logoText":"#fff",
        "currentVisitorSpan":"#777",
        "breadcrumbBorderBottom":"#eee",
        "breadcrumbList":"#777"
}); */
