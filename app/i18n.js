/**
 * i18n.js
 *
 * This will setup the i18n language files and locale data for your app.
 *
 */
import { addLocaleData } from 'react-intl';
import enLocaleData from 'react-intl/locale-data/en';
import { execute } from '@sigma-infosolutions/uplink/sagas/uplink/uplink';

import { DEFAULT_LOCALE } from './containers/App/constants'; // eslint-disable-line
import enTranslationMessages from './translations/en.json';

export const appLocales = [
  'en',
];

addLocaleData(enLocaleData);

export const formatTranslationMessages = (locale, messages) => {
  const defaultFormattedMessages = locale !== DEFAULT_LOCALE
    ? formatTranslationMessages(DEFAULT_LOCALE, enTranslationMessages)
    : {};
  return Object.keys(messages).reduce((formattedMessages, key) => {
    let message = messages[key];
    if (!message && locale !== DEFAULT_LOCALE) {
      message = defaultFormattedMessages[key];
    }
    return Object.assign(formattedMessages, { [key]: message });
  }, {});
};

export const translationMessages = () => new Promise((resolve) => {
  execute(undefined, undefined, 'configuration', 'i18n', 'fetch', { key: 'borrower' }).then(({ body }) => {
    try {
      if (!body.en) {
      body.en = enTranslationMessages; // eslint-disable-line
      }
      const locals = Object.keys(body);

      const abc = locals.reduce((a, c) => ({ [c]: formatTranslationMessages(c, body[c]) }), {});
      resolve(abc);
    } catch (e) {
      throw e;
    }
  })
  .catch(() => {
    resolve({
      en: formatTranslationMessages('en', enTranslationMessages),
    });
  });
});
