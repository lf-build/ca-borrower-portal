
export function maskBusinessTaxId(value) {
  let result = value;
  if (value.length === 9) {
    result = value.replace(/(\d{2})(\d{3})/, '$1-$2');
  }
  return result;
}

