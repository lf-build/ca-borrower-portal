export const ein = [/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
// export const ein = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
export const sic = [/\d/, /\d/, /\d/, /\d/];
export const usaZipCode = [/\d/, /\d/, /\d/, /\d/, /\d/];
const alphaNum = /[A-Z0-9]/;
export const canadaZipCode = [alphaNum, alphaNum, alphaNum, alphaNum, alphaNum, alphaNum];